from setuptools import setup, find_packages

def readme():
    with open("README.md") as f:
        return f.read()
    
setup(name='gesture_recognition',
      version='0.1',
      description='Gesture Recognition using Deep Learning on Leap Motion sensor data.',
      long_description=readme(),
      classifiers=[],
      url='',
      author='Chinmaya R Naguri',
      author_email='cn262114@ohio.edu',
      license='Other/Proprietory',
      packages=find_packages(), #['gesture_recognition'],
      install_requires=[],
      include_package_data=True,
      zip_safe=False)
