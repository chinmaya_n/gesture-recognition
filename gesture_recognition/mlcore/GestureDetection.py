from __future__ import print_function

import theano, numpy
import theano.tensor as tensor
from theano.ifelse import ifelse
from collections import OrderedDict

class GestureDetection(object):
    """
    Model for detecting if a gesture is happening in a given frame (a Leap Motion Frame)
    """

    def __init__(self, config, learned_params=None):
        """
        Gesture Detection init function. Compile the model and keep it ready to use.
        """
        # model configuration parameters
        self.frame_length = config.frame_size
        self.input_dim = self.frame_length # config.input_dim
        self.hidden_dim = config.memory_size
        self.optimizer = config.optimizer
        self.regularization_lambda = config.regularization_lambda
        self.log_offset = config.log_offset
        self.num_frame_types = config.number_of_frame_types
        self.floatX = theano.config.floatX

        # model params
        self.shared_params = self.init_shared_params()
        if learned_params:
            self.zipp(learned_params)
        self.other_params = self.init_other_params()
        # build the model
        x, y, self.f_pred_prob, self.f_pred, cost, self.f_lstm_cell_output = self.build_model()
        # regularization
        if self.regularization_lambda > 0:
            r_lambda = theano.shared(self.regularization_lambda, name="r_lambda")
            weight_decay = 0.
            weight_decay += (self.shared_params['softmax_W'] ** 2).sum()
            weight_decay *= r_lambda
            cost += weight_decay
        # gradients
        gradients = theano.grad(cost, wrt=self.shared_params.values())
        # optimization
        lr = tensor.scalar(name="lr")
        self.f_gradients_shared, self.f_update = self.adadelta(lr, self.shared_params, \
                                                               gradients, x, y, cost)

    def slice(self, param, slice_no, height):
        """
        Slice the given param based on height and slice_no
        """
        return param[slice_no*height : (slice_no+1)*height]

    def init_matrix(self, num_rows, num_cols):
        """
        Initialize a matrix with given dimentions
        """
        if num_rows == num_cols:
            mat_M = numpy.random.randn(num_rows, num_cols)
            mat_U, mat_Sigma, mat_V = numpy.linalg.svd(mat_M)
            return mat_U.astype(self.floatX)
        else:   # if rows and cols are not same
            if num_rows < num_cols: # we will not have all orthogonal vectors
                mat_L = self.init_matrix(num_rows, num_rows)
                mat_R = mat_L[:, 0:num_cols-num_rows]
                return numpy.concatenate((mat_L, mat_R), axis=1)
            else:
                mat_total = self.init_matrix(num_cols, num_cols)
                return mat_total[:, 0:num_cols]

    def init_shared_params(self):
        """
        Initialize shared parameters:
        W, U, b => lstm cell
        softmax1_W, softmax1_b => frame type classification softmax
        """
        shared_params = OrderedDict()

        # Parameters for LSTM cell
        lstm_W = numpy.concatenate((self.init_matrix(self.hidden_dim, self.input_dim),
                                    self.init_matrix(self.hidden_dim, self.input_dim),
                                    self.init_matrix(self.hidden_dim, self.input_dim),
                                    self.init_matrix(self.hidden_dim, self.input_dim)),
                                   axis=0)
        shared_params['W'] = theano.shared(lstm_W, name="W")
        lstm_U = numpy.concatenate((self.init_matrix(self.hidden_dim, self.input_dim),
                                    self.init_matrix(self.hidden_dim, self.input_dim),
                                    self.init_matrix(self.hidden_dim, self.input_dim),
                                    self.init_matrix(self.hidden_dim, self.input_dim)),
                                   axis=0)
        shared_params['U'] = theano.shared(lstm_U, name="U")
        lstm_b = numpy.zeros((4*self.hidden_dim, ), dtype=self.floatX)
        shared_params['b'] = theano.shared(lstm_b, name="b")

        # parameters for frame type softmax classifier
        softmax1_W = 0.01 * numpy.random.randn(self.num_frame_types, self.hidden_dim).astype(self.floatX)
        shared_params['softmax_W'] = theano.shared(softmax1_W, name="softmax_W")
        softmax1_b = numpy.zeros((self.num_frame_types, ), dtype=self.floatX)
        shared_params['softmax_b'] = theano.shared(softmax1_b, name="softmax_b")

        # return
        return shared_params

    def init_other_params(self):
        """
        Other shared parameters used in the model
        ht, ct, gt, frame_labels
        """
        # initialize variables
        ht = numpy.zeros((self.hidden_dim, 1), dtype=self.floatX)
        ct = numpy.zeros((self.hidden_dim, 1), dtype=self.floatX)

        cost_factor = 4.0
        frame_labels = numpy.asarray([0,0,0,0,0,1,1,1,1,0,0,0,0,0], dtype='int16')
        # theano shared variables dict
        other_params = OrderedDict()
        other_params['prev_ht'] = theano.shared(ht, name="prev_ht")
        other_params['prev_ct'] = theano.shared(ct, name="prev_ct")
        #other_params['gt'] = theano.shared(gt.transpose(), name="gt")
        other_params['frame_labels'] = theano.shared(frame_labels, name="frame_labels")
        other_params['inside_cost_factor'] = theano.shared(cost_factor, name="begin_cost_factor")
        # return
        return other_params

    def lstm_cell(self, input_frame):
        """
        An LSTM cell
        """
        # prev cell results
        ht_1 = self.other_params['prev_ht']
        ct_1 = self.other_params['prev_ct']
        # pre activation
        pre_activation = tensor.dot(self.shared_params['W'], input_frame).transpose() + self.shared_params['b']
        pre_activation += tensor.dot(self.shared_params['U'], ht_1).transpose()
        # neural networks
        height = self.hidden_dim
        pre_activation = pre_activation.transpose() # transpose to make it a column matrix
        ft = tensor.nnet.sigmoid(self.slice(pre_activation, 0, height))
        it = tensor.nnet.sigmoid(self.slice(pre_activation, 1, height))
        c_t = tensor.tanh(self.slice(pre_activation, 2, height))
        ot = tensor.nnet.sigmoid(self.slice(pre_activation, 3, height))
        # final calculations for this cell
        ct = ft * ct_1 + it * c_t
        ht = ot * tensor.tanh(ct)

        # return
        return ht, ct

    def build_model(self):
        """
        Build the complete neural network model and return the symbolic variables
        """
        # symbolic variables
        x = tensor.matrix(name="x", dtype=self.floatX)
        y = tensor.iscalar(name="y")

        # lstm cell
        (ht, ct) = self.lstm_cell(x)  # gets the ht, ct
        # softmax 1 i.e. frame type prediction
        activation = tensor.dot(self.shared_params['softmax_W'], ht).transpose() + self.shared_params['softmax_b']
        frame_pred = tensor.nnet.softmax(activation) # .transpose()

        # predicted probability for frame type
        f_pred_prob = theano.function([x], frame_pred, name="f_pred_prob")
        # predicted frame type
        f_pred = theano.function([x], frame_pred.argmax(), name="f_pred")

        # cost
        cost = ifelse(tensor.eq(y, 0), -tensor.log(frame_pred[0,0]+self.log_offset),
                      ifelse(tensor.eq(y, 1), -tensor.log(frame_pred[0,1]+self.log_offset)
                             * self.other_params['inside_cost_factor'],
                             10000.012), name="cost_ifelse")
        # some random value (10000.012) is used for frames with wrong labels. It almost never comes to last else.

        # function for output of the currect lstm cell
        f_lstm_cell_output = theano.function([x], (ht, ct), name="f_model_cell_output")
        # return the model symbolic variables and theano functions
        return x, y, f_pred_prob, f_pred, cost, f_lstm_cell_output

    def numpy_floatX(self, data):
        """
        Converts the given 'data' into numpy array and as type theano.config.floatX
        """
        return numpy.asarray(data, dtype=theano.config.floatX)

    def adadelta(self, lr, tparams, grads, x, y, cost):
        """
        An adaptive learning rate optimizer

        Parameters
        ----------
        lr : Theano SharedVariable
            Initial learning rate
        tpramas: Theano SharedVariable
            Model parameters
        grads: Theano variable
            Gradients of cost w.r.t to parameres
        x: Theano variable
            Model inputs
        y: Theano variable
            Targets
        cost: Theano variable
            Objective fucntion to minimize

        Notes
        -----
        For more information, see [ADADELTA]_.

        .. [ADADELTA] Matthew D. Zeiler, *ADADELTA: An Adaptive Learning
           Rate Method*, arXiv:1212.5701.
        """

        zipped_grads = [theano.shared(p.get_value() * self.numpy_floatX(0.),
                                      name='%s_grad' % k)
                        for k, p in tparams.items()]
        running_up2 = [theano.shared(p.get_value() * self.numpy_floatX(0.),
                                     name='%s_rup2' % k)
                       for k, p in tparams.items()]
        running_grads2 = [theano.shared(p.get_value() * self.numpy_floatX(0.),
                                        name='%s_rgrad2' % k)
                          for k, p in tparams.items()]

        zgup = [(zg, g) for zg, g in zip(zipped_grads, grads)]
        rg2up = [(rg2, 0.95 * rg2 + 0.05 * (g ** 2))
                 for rg2, g in zip(running_grads2, grads)]

        # debug
        def pre_f(i, node, fn):
            print(i, node, "inputs: ", [numpy.asarray(inp).shape for inp in fn.inputs], end=' ')
        def post_f(i, node, fn):
            print("outputs: ", [numpy.asanyarray(outp).shape for outp in fn.outputs])
        mode = theano.compile.MonitorMode(pre_func=pre_f, post_func=post_f) #.excluding('local_elemwise_fusion', 'inplace')
        # /debug
        f_grad_shared = theano.function([x, y], cost, updates=zgup + rg2up,
                                        name='adadelta_f_grad_shared')

        updir = [-tensor.sqrt(ru2 + 1e-6) / tensor.sqrt(rg2 + 1e-6) * zg
                 for zg, ru2, rg2 in zip(zipped_grads,
                                         running_up2,
                                         running_grads2)]
        ru2up = [(ru2, 0.95 * ru2 + 0.05 * (ud ** 2))
                 for ru2, ud in zip(running_up2, updir)]
        param_up = [(p, p + ud) for p, ud in zip(tparams.values(), updir)]

        f_update = theano.function([lr], [], updates=ru2up + param_up,
                                   on_unused_input='ignore',
                                   name='adadelta_f_update')

        return f_grad_shared, f_update

    def sgd(self, lr, tparams, grads, x, y, cost):
        """ Stochastic Gradient Descent

        :note: A more complicated version of sgd then needed.  This is
            done like that for adadelta and rmsprop.

        """
        # New set of shared variable that will contain the gradient
        # for a mini-batch.
        gshared = [theano.shared(p.get_value() * 0., name='%s_grad' % k)
                   for k, p in tparams.items()]
        gsup = [(gs, g) for gs, g in zip(gshared, grads)]

        # Function that computes gradients for a mini-batch, but do not
        # updates the weights.
        f_grad_shared = theano.function([x, y], cost, updates=gsup,
                                        name='sgd_f_grad_shared')

        pup = [(p, p - lr * g) for p, g in zip(tparams.values(), gshared)]

        # Function that updates the weights from the previously computed
        # gradient.
        f_update = theano.function([lr], [], updates=pup,
                                   name='sgd_f_update')

        return f_grad_shared, f_update

    def zipp(self, params):
        """
        Zips the given params into shared parameters. This will be useful when
        we reload the model.

        Parameters
        ----------
        params: a dictionary of variables (parameters for the model)
        shared_params: a theano shared variables dictionary. (values from params
        will be filled into this)

        Return/Output
        -------------
        Fills the shared_params with corresponding values in params
        """
        for key, value in params.items():
            if key not in ['test_acc', 'history_accu']:
                self.shared_params[key].set_value(value)    
