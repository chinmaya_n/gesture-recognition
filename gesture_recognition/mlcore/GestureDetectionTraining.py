from __future__ import print_function

from random import shuffle
from collections import Counter
from enum import Enum
import six.moves.cPickle as pickle
import time, os, sys, numpy

from gesture_recognition.mlcore.GestureDetection import GestureDetection

class FrameType(Enum):
    """
    Enumerator class for Frame types
    """
    outside = 0
    inside = 1

class GestureDetectionTraining(object):
    """
    Train the Gesture Detection model for data from Leap Motion controller.
    """

    def __init__(self, config):
        """
        Initialization function for the GestureDetectionTraining object.
        """
        # training parameters
        self.learning_rate = config.learning_rate
        self.patience = config.patience
        self.max_epochs = config.maximum_epochs
        self.disp_freq = config.log_display_frequency
        self.validation_freq = config.validation_frequency
        self.params_save_freq = config.params_save_frequency
        self.label_slide_width = config.label_slide_width
        self.config = config

        # files to save and get data from
        self.save_params_to = config.save_params_to
        #self.frames_data_file = config.frames_data_file
        #self.frames_labels_file = config.frames_labels_file

        # Gesture Detection
        self.gd = GestureDetection(config)

        # get the data
        #self.data = self.get_data(self.frames_data_file, self.frames_labels_file, self.gd.frame_length)
        self.train_data = self.get_data(config.gd_train_frames_file, config.gd_train_labels_file, self.gd.frame_length) #(self.data[0][0:65], self.data[1][0:65])
        self.validation_data = self.get_data(config.gd_valid_frames_file, config.gd_valid_labels_file, self.gd.frame_length) #(self.data[0][65:80], self.data[1][65:80])
        self.test_data = self.get_data(config.gd_test_frames_file, config.gd_test_labels_file, self.gd.frame_length) #(self.data[0][80:], self.data[1][80:])

        # train
        self.train_model()

    def unzip(self, zipped):
        """
        Unzip the zipped. This makes a copy of the shared theano variables
        as normal variables.

        Parameters
        ----------
        zipped: shared theano variables dictionary

        Return/Output
        -------------
        a normal variable dictionary with keys as names values as their values
        """
        params = {}
        for key in zipped.keys():
            params[key] = zipped[key].get_value()
        return params

    def zipp(self, params, shared_params):
        """
        Zips the given params into shared parameters. This will be useful when
        we reload the model.

        Parameters
        ----------
        params: a dictionary of variables (parameters for the model)
        shared_params: a theano shared variables dictionary. (values from params
        will be filled into this)

        Return/Output
        -------------
        Fills the shared_params with corresponding values in params
        """
        for key, value in params.items():
            shared_params[key].set_value(value)

    def prediction_accuracy(self, data):
        """
        Predict the labels for the given data and return accuracy.

        > there are some known mistakes or repetitive counts here.
        > boundaries of pred array is not checked
        """
        total_frames = 0
        pred_correct_count = 0.0
        pred_wrong_count = 0.0

        # predict labels for the data
        all_pred_labels = self.predict_labels(data)

        # for each example
        for ex_id in range(data[1].shape[0]):
            # example and frame ids
            ex_x = data[0][ex_id]
            ex_y = data[1][ex_id]
            pred_labels = all_pred_labels[ex_id]

            # add to total frames
            total_frames += len(pred_labels)
            # cost factors
            count = Counter(ex_y)
            inside_cost_factor = count[FrameType.outside.value]/(1.0*count[FrameType.inside.value])

            # compare true and pred labels
            num_frames = len(ex_y)
            for label_id in range(num_frames):
                # outside
                if ex_y[label_id]==FrameType.outside.value:
                    if ex_y[label_id]==pred_labels[label_id]:
                        pred_correct_count += 1
                    else:
                        pred_wrong_count += inside_cost_factor
                # inside
                elif ex_y[label_id]==FrameType.inside.value:
                    if ex_y[label_id]==pred_labels[label_id]:
                        pred_correct_count += inside_cost_factor
                    else:
                        pred_wrong_count += inside_cost_factor

        # return
        return (100*pred_correct_count)/(pred_correct_count+pred_wrong_count)

    def predict_labels(self, data):
        """
        Predict the labels for the given data.
        """
        labels = []
        # for each example
        for ex_id in range(data[1].shape[0]):
            # example and frame ids
            ex_x = data[0][ex_id]
            ex_y = data[1][ex_id]
            pred_labels = []

            # reset the memory cells for each new example, as they are independent
            self.gd.other_params['prev_ct'].set_value(numpy.zeros((self.gd.hidden_dim, 1), dtype=self.gd.floatX))

            # predict labels for each frame
            for frame_id in range(len(ex_y)):
                frame = numpy.matrix(ex_x[:,frame_id]).transpose()
                pred_labels.append(self.gd.f_pred(frame))

                # update the ht, ct values of prev cell
                (ht, ct) = self.gd.f_lstm_cell_output(frame)
                self.gd.other_params['prev_ht'].set_value(ht)
                self.gd.other_params['prev_ct'].set_value(ct)
            #
            labels.append(pred_labels)
        # return
        return labels

    def slide_array(self, labels, n):
        """
        Slide array by 'n' positions to the right by keeping the length of the array same.
        Mark the first frames as 'outside' frames or label 3
        """
        if n>0:
            result = []
            for l in labels:
                ans1 = numpy.repeat([0], n)
                ans2 = l[:-n]
                result.append(numpy.concatenate((ans1, ans2)).tolist())
            return numpy.array(result)
        else:
            return labels

    def get_data(self, frames_file, labels_file, frame_length):
        """
        Gets the data from the file.
        """
        # frames file
        fp = open(frames_file, 'r')
        example_count = int(fp.readline())
        data = ([], [])
        for line in fp.readlines():
            line = [float(num) for num in line.split()]
            example = numpy.array(line[0:]).reshape((len(line))/frame_length, frame_length)
            data[0].append(example.transpose())
        fp.close()
        # frame labels file
        fp = open(labels_file, 'r')
        example_count = int(fp.readline())
        for line in fp.readlines():
            line = [int(num) for num in line.split()]
            data[1].append(line)
        fp.close()
        # data
        data = (data[0], self.slide_array(numpy.array(data[1]), self.label_slide_width))
        return data

    def train_model(self):
        """
        Train the Gesture Detection model for given data and parameters.
        """

        epoch_count = 0
        updates_count = 0
        bad_counter = 0
        history_accu = []
        best_params = None
        early_stop = False
        start_time = time.time()

        try:
            # train in each epoch until max_epochs or early stop
            for epoch_id in range(self.max_epochs):
                epoch_count += 1
                random_ids = range(len(self.train_data[0])) # ids from training examples
                shuffle(random_ids)               # randomize the ids

                # for each training example in training data
                for train_ex_id in random_ids:
                    # example and frame ids
                    ex_x = self.train_data[0][train_ex_id]
                    ex_y = self.train_data[1][train_ex_id]
                    self.gd.other_params['frame_labels'].set_value(ex_y)
                    # calculate cost factors for this example
                    count = Counter(ex_y)
                    inside_factor = count[FrameType.outside.value]/(1.0*count[FrameType.inside.value])
                    self.gd.other_params['inside_cost_factor'].set_value(inside_factor)
                    # reset the memory cells
                    self.gd.other_params['prev_ct'].set_value(numpy.zeros((self.gd.hidden_dim, 1), dtype=self.gd.floatX))

                    # iterate over each frame of the example
                    for frame_id in range(len(ex_y)):
                        updates_count += 1
                        # frame example and id
                        ex_frame_x = ex_x[:, frame_id]
                        ex_frame_y = ex_y[frame_id]
                        ex_frame_x = numpy.asarray([ex_frame_x]).transpose()

                        # cost
                        cost = self.gd.f_gradients_shared(ex_frame_x, ex_frame_y)
                        # update the parameters
                        self.gd.f_update(self.learning_rate)
                        # update the prev lstm cell output
                        (ht, ct) = self.gd.f_lstm_cell_output(ex_frame_x)
                        self.gd.other_params['prev_ht'].set_value(ht)
                        self.gd.other_params['prev_ct'].set_value(ct)

                        # check if the cost is valid
                        if(numpy.isnan(cost) and numpy.isinf(cost)):
                            print("Bad cost detected... %d. Exiting" % cost)
                            return 1., 1., 1.
                        # display training iteration information
                        if(numpy.mod(updates_count, self.disp_freq) == 0):
                            print('Epoch: ', epoch_id, 'Updates: ', updates_count, 'Cost: ', cost)
                        # save the parameters
                        if self.save_params_to and numpy.mod(updates_count, self.params_save_freq) == 0:
                            print("Saving the parameters... %s" % self.save_params_to)
                            if best_params is not None:
                                params = best_params
                            else:
                                params = self.unzip(self.gd.shared_params)
                            numpy.savez(self.save_params_to, history_accu=history_accu, **params)
                            pickle.dump(self.config, open('%s.pkl' % self.save_params_to, 'wb'), -1)
                            print('Done.')
                        # intermediate validation data performance and early stop check
                        if numpy.mod(updates_count, self.validation_freq) == 0:
                            train_acc = self.prediction_accuracy(self.train_data)
                            valid_acc = self.prediction_accuracy(self.validation_data)
                            history_accu.append([train_acc, valid_acc])
                            # check for best params
                            if best_params is None or \
                               valid_acc >= numpy.array(history_accu)[:, 1].max():
                                best_params = self.unzip(self.gd.shared_params)
                                bad_counter = 0
                            print('________________')
                            print('Train: ', train_acc, 'Valid: ', valid_acc)
                            print('________________')
                            # early stop check
                            if len(history_accu) > self.patience and \
                               valid_acc <= numpy.array(history_accu)[:-self.patience, 1].max():
                                bad_counter += 1
                                if bad_counter >= self.patience:
                                    print('Early Stop!')
                                    early_stop = True
                                    break

                    if early_stop:
                        break
                if early_stop:
                    break

        except KeyboardInterrupt:
            print("Training stopped... Keyboard Interrupt.")

        end_time = time.time()
        # get the best parameters
        if best_params is not None:
            self.zipp(best_params, self.gd.shared_params)
        else:
            best_params = self.unzip(self.gd.shared_params)

        # performance on test data
        print('________________')
        test_acc = self.prediction_accuracy(self.test_data)
        print('Test Accuracy: ', test_acc, '\n')

        # save the results and parameters
        if self.save_params_to:
            numpy.savez(self.save_params_to, test_acc = test_acc, **best_params)

        # time taken
        total_time = end_time - start_time
        print('The code ran for %d epochs and took %d sec/epoch' % (epoch_count, \
              total_time/(epoch_count*1.0)))
        print('Total time taken: %d secs' % total_time, file=sys.stderr)
        return train_acc, valid_acc, test_acc
