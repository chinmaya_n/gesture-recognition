from __future__ import print_function
import numpy
from gesture_recognition.mlcore.GestureClassification import GestureClassification

class GestureClassificationPredict(object):
    """
    Gesture Classification for a given set of frames. We will classify it
    as any one of the learned gestures.
    """
    
    def __init__(self, config, paramsFile):
        """
        Initializatin function while creating GestureClassificationPredict object.
        """
        # load the model params
        modelParams = numpy.load(paramsFile)
        # get GestureClassification object
        self.gc = GestureClassification(config, modelParams)
    
    def predict_label(self, frames):
        """
        Predict label for the gesture in the given frames.
        """
        # send the frames to predict and return the label
        return self.gc.f_pred(frames)
    
    def predict_prob(self, frames):
        """
        Predict the probability of a gesture present in given frames.
        """
        # send the frames to predict probabilities of each gesture happening
        return self.gc.f_pred_prob(frames)
