from __future__ import print_function

import numpy
from collections import OrderedDict
from gesture_recognition.mlcore.GestureDetection import GestureDetection

class GestureDetectionPredict(object):
    """
    Predicts if a frame has any gesture.
    """
    
    def __init__(self, config, paramsFile):
        """
        Initialization function for GestureDetectionPredict object.
        """
        # load params
        modelParams = numpy.load(paramsFile)
        # build the model
        self.gd = GestureDetection(config, modelParams)
    
    def predict_label(self, frame):
        """
        Predict the label for the given frame.
        """
        # label
        label = self.gd.f_pred(frame)
        # update the ht, ct values of prev cell
        (ht, ct) = self.gd.f_lstm_cell_output(frame)
        self.gd.other_params['prev_ht'].set_value(ht)
        self.gd.other_params['prev_ct'].set_value(ct)        
        # return
        return label
    
    def predict_prob(self, frame):
        """
        Predict the probabilities of a frame being outside/inside.
        """
        # label
        prob = self.gd.f_pred_prob(frame)
        # update the ht, ct values of prev cell
        (ht, ct) = self.gd.f_lstm_cell_output(frame)
        self.gd.other_params['prev_ht'].set_value(ht)
        self.gd.other_params['prev_ct'].set_value(ct)        
        # return
        return prob

    def reset_memory_cells(self):
        """
        Reset the LSTM memory cells of the model. Used when the upcoming frames are not
        in sequence with the previous frames & independent.
        """
        self.gd.other_params['prev_ct'].set_value(numpy.zeros((self.gd.hidden_dim, 1), 
                                                              dtype=self.gd.floatX))
