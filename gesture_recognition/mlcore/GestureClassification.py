from __future__ import print_function

import theano, numpy
import theano.tensor as tensor
from theano.ifelse import ifelse
from collections import OrderedDict

class GestureClassification(object):
    """
    Model for classifing a given gesture.
    """

    def __init__(self, config, learned_params=None):
        """
        Gesture Classification init function. Compile the model and keep it ready.
        """

        # model configuration parameters
        self.frame_length = config.frame_size
        self.input_dim = self.frame_length # config.input_dim
        self.hidden_dim = config.gc_memory_size
        self.optimizer = config.optimizer
        self.regularization_lambda = config.gc_regularization_lambda
        self.log_offset = config.gc_log_offset
        self.num_classes = config.number_of_gesture_types
        self.floatX = theano.config.floatX

        # model params
        self.shared_params = self.init_shared_params()
        if learned_params:
            self.zipp(learned_params)
        # build model
        x, y, self.f_pred_prob, self.f_pred, cost = self.build_model()
        # weight regularization in softmax
        if self.regularization_lambda > 0:
            r_lambda = theano.shared(self.regularization_lambda, name='r_lambda')
            weight_decay = 0.
            weight_decay += (self.shared_params['softmax_W'] ** 2).sum()
            weight_decay *= r_lambda
            cost += weight_decay
        # gradients
        gradients = theano.grad(cost, wrt=self.shared_params.values())
        # optimizer
        lr = tensor.scalar(name='lr')
        self.f_gradients_shared, self.f_update = self.adadelta(lr, self.shared_params, gradients, x, y, cost)

    def lstm_layer(self, input_ex):
        """
        LSTM Layer implementation. (Variable Length inputs)

        Parameters
        ----------
        shared_params: shared model parameters W, U, b etc
        input_ex: input example (say dimension: 36 x 100 i.e 36 features and 100 time units)
        options: Neural Network model options

        Output / returns
        ----------------
        output of each lstm cell [h_0, h_1, ..... , h_t]
        """

        def slice(param, slice_no, height):
            return param[slice_no*height : (slice_no+1)*height]

        def cell(wxb_row, ht_1, ct_1):
            #ht_1 = numpy.asarray
            pre_activation = tensor.dot(self.shared_params['U'], ht_1)
            pre_activation += wxb_row.transpose()

            height = self.hidden_dim
            ft = tensor.nnet.sigmoid(slice(pre_activation, 0, height))
            it = tensor.nnet.sigmoid(slice(pre_activation, 1, height))
            c_t = tensor.tanh(slice(pre_activation, 2, height))
            ot = tensor.nnet.sigmoid(slice(pre_activation, 3, height))

            ct = ft * ct_1 + it * c_t
            ht = ot * tensor.tanh(ct)

            return ht, ct

        num_frames = input_ex.shape[1]
        wxb = tensor.dot(self.shared_params['W'], input_ex).transpose() + self.shared_params['b']
        result, updates = theano.scan(cell,
                                      sequences=[wxb],
                                      outputs_info=[tensor.alloc(numpy.asarray(0., dtype=self.floatX),
                                                                 self.hidden_dim),
                                                    tensor.alloc(numpy.asarray(0., dtype=self.floatX),
                                                                 self.hidden_dim)],
                                      n_steps=num_frames)

        #tensor.zeros_like(tensor.as_tensor(numpy.zeros((36,1), dtype=floatX))),
        #tensor.zeros_like(tensor.as_tensor(numpy.zeros((36,1), dtype=floatX)))
        return result[0].transpose()  # only ht is needed

    def build_model(self):
        """
        Build the complete neural network model and return the symbolic variables

        Parameters
        ----------
        shared_params: shared, model parameters W, U, b etc
        options: Neural Network model options

        return
        ------
        x, y, f_pred_prob, f_pred, cost
        """

        x = tensor.matrix(name='x', dtype=self.floatX)
        y = tensor.iscalar(name='y') # tensor.vector(name='y', dtype=floatX)

        num_frames = x.shape[1]
        # lstm outputs from each cell
        lstm_result = self.lstm_layer(x)
        # mean pool from the lstm cell outputs
        pool_result = lstm_result.sum(axis=1)/(1. * num_frames)
        # Softmax / Logistic Regression
        pred = tensor.nnet.softmax(tensor.dot(self.shared_params['softmax_W'], pool_result) +
                                   self.shared_params['softmax_b'])
        #theano.printing.debugprint(pred)
        # predicted probability function
        f_pred_prob = theano.function([x], pred, name='f_pred_prob', mode='DebugMode') # 'DebugMode'
        # predicted class
        f_pred = theano.function([x], pred.argmax(axis=1), name='f_pred')
        # cost of the model: -ve log likelihood
        cost = -tensor.log(pred[0, y-1] + self.log_offset)    # y = 1,2,...n but indexing is 0,1,..(n-1)

        return x, y, f_pred_prob, f_pred, cost

    def orthogonal_init(self, num_rows, num_cols):
        """
        Orthogonal Initialization of a matrix with given rows, columns.
        A good discussion is here: http://bit.ly/1oyXnVU

        Parameters:
        -----------
        number of rows, columns in the matrix

        return/output:
        --------------
        a matrix with given no. of rows, cols (it is 'U' from SVD)
        """
        if num_rows == num_cols:
            mat_M = numpy.random.randn(num_rows, num_cols)
            mat_U, mat_Sigma, mat_V = numpy.linalg.svd(mat_M)
            return mat_U.astype(self.floatX)
        else:   # if rows and cols are not same
            if num_rows < num_cols: # we will not have all orthogonal vectors
                mat_L = self.init_matrix(num_rows, num_rows)
                mat_R = mat_L[:, 0:num_cols-num_rows]
                return numpy.concatenate((mat_L, mat_R), axis=1)
            else:
                mat_total = self.init_matrix(num_cols, num_cols)
                return mat_total[:, 0:num_cols]

    def init_shared_params(self):
        """
        Initialize shared parameters W, U, b and softmax_W, softmax_b

        Parameters:
        ----------
        input_dim: main dim of the input vector i.e size:36x1 input_dim=36
        hidden_dim: number of hidden neurons we want = 100 or 72
        num_classes: number of classes in classificatin task i.e in softmax = 2 or 5

        returns/output:
        --------------
        a dict with all shared parameters with values initialized.
        """
        shared_params = OrderedDict() #{}
        # Parameters for LSTM ; All gates weights concatinated
        lstm_W = numpy.concatenate((self.orthogonal_init(self.hidden_dim, self.input_dim),
                                   self.orthogonal_init(self.hidden_dim, self.input_dim),
                                   self.orthogonal_init(self.hidden_dim, self.input_dim),
                                   self.orthogonal_init(self.hidden_dim, self.input_dim)),
                                  axis=0)
        shared_params['W'] = theano.shared(lstm_W, name="W")
        lstm_U = numpy.concatenate((self.orthogonal_init(self.hidden_dim, self.hidden_dim),
                                   self.orthogonal_init(self.hidden_dim, self.hidden_dim),
                                   self.orthogonal_init(self.hidden_dim, self.hidden_dim),
                                   self.orthogonal_init(self.hidden_dim, self.hidden_dim)),
                                  axis=0)
        shared_params['U'] = theano.shared(lstm_U, name="U")
        lstm_b = numpy.zeros((4*self.hidden_dim, ), dtype=self.floatX)
        shared_params['b'] = theano.shared(lstm_b, name="b")
        # Parameters for Softmax / Classifier
        softmax_W = 0.01 * numpy.random.randn(self.num_classes, self.hidden_dim).astype(self.floatX)
        shared_params['softmax_W'] = theano.shared(softmax_W, name="softmax_W")
        softmax_b = numpy.zeros((self.num_classes, ), dtype=self.floatX)
        shared_params['softmax_b'] = theano.shared(softmax_b, name="softmax_b")
        # return the shared parameters dict
        return shared_params

    def numpy_floatX(self, data):
        return numpy.asarray(data, dtype=self.floatX)

    def sgd(self, lr, tparams, grads, x, y, cost):
        """ Stochastic Gradient Descent

        :note: A more complicated version of sgd then needed.  This is
            done like that for adadelta and rmsprop.

        """
        # New set of shared variable that will contain the gradient
        # for a mini-batch.
        gshared = [theano.shared(p.get_value() * 0., name='%s_grad' % k)
                   for k, p in tparams.items()]
        gsup = [(gs, g) for gs, g in zip(gshared, grads)]

        # Function that computes gradients for a mini-batch, but do not
        # updates the weights.
        f_grad_shared = theano.function([x, y], cost, updates=gsup,
                                        name='sgd_f_grad_shared')

        pup = [(p, p - lr * g) for p, g in zip(tparams.values(), gshared)]

        # Function that updates the weights from the previously computed
        # gradient.
        f_update = theano.function([lr], [], updates=pup,
                                   name='sgd_f_update')

        return f_grad_shared, f_update

    def adadelta(self, lr, tparams, grads, x, y, cost):
        """
        An adaptive learning rate optimizer

        Parameters
        ----------
        lr : Theano SharedVariable
            Initial learning rate
        tpramas: Theano SharedVariable
            Model parameters
        grads: Theano variable
            Gradients of cost w.r.t to parameres
        x: Theano variable
            Model inputs
        y: Theano variable
            Targets
        cost: Theano variable
            Objective fucntion to minimize

        Notes
        -----
        For more information, see [ADADELTA]_.

        .. [ADADELTA] Matthew D. Zeiler, *ADADELTA: An Adaptive Learning
           Rate Method*, arXiv:1212.5701.
        """

        zipped_grads = [theano.shared(p.get_value() * self.numpy_floatX(0.),
                                      name='%s_grad' % k)
                        for k, p in tparams.items()]
        running_up2 = [theano.shared(p.get_value() * self.numpy_floatX(0.),
                                     name='%s_rup2' % k)
                       for k, p in tparams.items()]
        running_grads2 = [theano.shared(p.get_value() * self.numpy_floatX(0.),
                                        name='%s_rgrad2' % k)
                          for k, p in tparams.items()]

        zgup = [(zg, g) for zg, g in zip(zipped_grads, grads)]
        rg2up = [(rg2, 0.95 * rg2 + 0.05 * (g ** 2))
                 for rg2, g in zip(running_grads2, grads)]

        # debug
        def pre_f(i, node, fn):
            print(i, node, "inputs: ", [numpy.asarray(inp).shape for inp in fn.inputs], end=' ')
        def post_f(i, node, fn):
            print("outputs: ", [numpy.asanyarray(outp).shape for outp in fn.outputs])
        mode = theano.compile.MonitorMode(pre_func=pre_f, post_func=post_f) #.excluding('local_elemwise_fusion', 'inplace')
        # /debug
        f_grad_shared = theano.function([x, y], cost, updates=zgup + rg2up,
                                        name='adadelta_f_grad_shared')

        updir = [-tensor.sqrt(ru2 + 1e-6) / tensor.sqrt(rg2 + 1e-6) * zg
                 for zg, ru2, rg2 in zip(zipped_grads,
                                         running_up2,
                                         running_grads2)]
        ru2up = [(ru2, 0.95 * ru2 + 0.05 * (ud ** 2))
                 for ru2, ud in zip(running_up2, updir)]
        param_up = [(p, p + ud) for p, ud in zip(tparams.values(), updir)]

        f_update = theano.function([lr], [], updates=ru2up + param_up,
                                   on_unused_input='ignore',
                                   name='adadelta_f_update')

        return f_grad_shared, f_update

    def zipp(self, params):
        """
        Zips the given params into shared parameters. This will be useful when
        we reload the model.
    
        Parameters
        ----------
        params: a dictionary of variables (parameters for the model)
        shared_params: a theano shared variables dictionary. (values from params
        will be filled into this)
    
        Return/Output
        -------------
        Fills the shared_params with corresponding values in params
        """
        for key, value in params.items():
            if key not in ['test_acc', 'history_accu']:
                self.shared_params[key].set_value(value)
