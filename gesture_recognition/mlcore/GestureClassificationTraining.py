from __future__ import print_function

from random import shuffle
from collections import Counter
from enum import Enum
import six.moves.cPickle as pickle
import time, os, sys, numpy

from gesture_recognition.mlcore.GestureClassification import GestureClassification

class GestureClassificationTraining(object):
    """
    Train the Gesture Classification model for data from Leap Motion controller.
    """

    def __init__(self, config):
        """
        Initialization function for the GestureClassificationTraining object.
        """
        # training parameters
        self.learning_rate = config.gc_learning_rate
        self.patience = config.gc_patience
        self.max_epochs = config.gc_maximum_epochs
        self.disp_freq = config.gc_log_display_frequency
        self.validation_freq = config.gc_validation_frequency
        self.params_save_freq = config.gc_params_save_frequency
        self.config = config

        # files to save and get data from
        self.save_params_to = config.gc_save_params_to
        #self.gestures_data_file = config.gestures_data_file

        # gesture classification
        self.gc = GestureClassification(config)

        # get the data
        #self.data = self.get_data(self.gestures_data_file, self.gc.frame_length)
        self.train_data = self.get_data(config.gc_train_gestures_file, self.gc.frame_length) #(self.data[0][0:700], self.data[1][0:700]+1)
        self.validation_data = self.get_data(config.gc_valid_gestures_file, self.gc.frame_length) #(self.data[0][700:900], self.data[1][700:900]+1)
        self.test_data = self.get_data(config.gc_test_gestures_file, self.gc.frame_length) #(self.data[0][900:], self.data[1][900:]+1)
        
        # label are starting from zero, So making it to start from one
        self.train_data = (self.train_data[0], self.train_data[1]+1)
        self.validation_data = (self.validation_data[0], self.validation_data[1]+1)
        self.test_data = (self.test_data[0], self.test_data[1]+1)
        
        # train
        self.train_model()

    def unzip(self, zipped):
        """
        Unzip the zipped. This makes a copy of the shared theano variables
        as normal variables.

        Parameters
        ----------
        zipped: shared theano variables dictionary

        Return/Output
        -------------
        a normal variable dictionary with keys as names values as their values
        """
        params = {}
        for key in zipped.keys():
            params[key] = zipped[key].get_value()
        return params

    def zipp(self, params, shared_params):
        """
        Zips the given params into shared parameters. This will be useful when
        we reload the model.

        Parameters
        ----------
        params: a dictionary of variables (parameters for the model)
        shared_params: a theano shared variables dictionary. (values from params
        will be filled into this)

        Return/Output
        -------------
        Fills the shared_params with corresponding values in params
        """
        for key, value in params.items():
            shared_params[key].set_value(value)

    def prediction_accuracy(self, data):
        """
        Predict the class of the given examples.

        Parameters
        ----------
        f_pred: theano function for the prediction
        data: A tuple. data[0] = examples; data[1] = labels

        Return/Output
        -------------
        Accuracy of the 'data'
        """
        accuracy = 0
        for item_id in range(len(data[0])):
            if(self.gc.f_pred(data[0][item_id])+1 == data[1][item_id]):
                accuracy += 1
        return 100*(accuracy/(1.*len(data[0])))

    def get_data(self, file_name, frame_length):
        """
        Gets the data from the file.

        Parameters
        ----------
        file_name: location of the file.
        frame_length: Length of each frame (frame dimension)

        Return/Output
        -------------
        A tuple. data[0] = list of matrices (examples)
                 data[1] = true labels
        """
        fp = open(file_name, 'r')
        example_count = int(fp.readline())
        data = ([], [])
        for line in fp.readlines():
            line = [float(num) for num in line.split()]
            data[1].append(int(line[0]))
            example = numpy.array(line[1:]).reshape((len(line))/frame_length, frame_length)
            data[0].append(example.transpose())
        data = (data[0], numpy.array(data[1]))
        return data

    def train_model(self):
        """
        Train the Gesture Classification model for given data and parameters.
        """
        # optimization with early stop
        epoch_count = 0
        updates_count = 0
        bad_counter = 0
        history_accu = []
        best_params = None
        early_stop = False
        start_time = time.time()

        try:
            # train in each epoch until max_epochs (or earlystop!)
            for epoch_id in range(self.max_epochs):
                epoch_count = epoch_id + 1
                random_ids = range(len(self.train_data[0]))
                shuffle(random_ids)  # shuffles the ids into random numbers

                # for each example in training data
                for item_id in random_ids:
                    updates_count += 1
                    # training example
                    ex_x = self.train_data[0][item_id]
                    ex_y = self.train_data[1][item_id]
                    # cost & update
                    cost = self.gc.f_gradients_shared(ex_x, ex_y)
                    self.gc.f_update(self.learning_rate)

                    # check if the cost is valid
                    if(numpy.isnan(cost) and numpy.isinf(cost)):
                        print("Bad cost detected... %d. Exiting" % cost)
                        return 1., 1., 1.
                    # display training iteration information
                    if(numpy.mod(updates_count, self.disp_freq) == 0):
                        print('Epoch: ', epoch_id, 'Updates: ', updates_count, 'Cost: ', cost)
                    # save the parameters
                    if self.save_params_to and numpy.mod(updates_count, self.params_save_freq) == 0:
                        print("Saving the parameters... %s" % self.save_params_to)
                        if best_params is not None:
                            params = best_params
                        else:
                            params = self.unzip(self.gc.shared_params)
                        numpy.savez(self.save_params_to, history_accu=history_accu, **params)
                        pickle.dump(self.config, open('%s.pkl' % self.save_params_to, 'wb'), -1)
                        print('Done.')
                    # intermediate validation data performance and early stop check
                    if numpy.mod(updates_count, self.validation_freq) == 0:
                        train_acc = self.prediction_accuracy(self.train_data)
                        valid_acc = self.prediction_accuracy(self.validation_data)
                        history_accu.append([train_acc, valid_acc])
                        # check for best params
                        if best_params is None or \
                           valid_acc >= numpy.array(history_accu)[:, 1].max():
                            best_params = self.unzip(self.gc.shared_params)
                            bad_counter = 0
                        print('________________')
                        print('Train: ', train_acc, 'Valid: ', valid_acc)
                        print('________________')
                        # early stop check
                        if len(history_accu) > self.patience and \
                           valid_acc <= numpy.array(history_accu)[:-self.patience, 1].max():
                            bad_counter += 1
                            if bad_counter >= self.patience:
                                print('Early Stop!')
                                early_stop = True
                                break

                if early_stop:
                    break

        except KeyboardInterrupt:
            print("Training stopped... Keyboard Interrupt.")

        end_time = time.time()
        # get the best parameters
        if best_params is not None:
            self.zipp(best_params, self.gc.shared_params)
        else:
            best_params = self.unzip(self.gc.shared_params)

        # performance on test data
        print('________________')
        test_acc = self.prediction_accuracy(self.test_data)
        print('Test Accuracy: ', test_acc, '\n')

        # save the results and parameters
        if self.save_params_to:
            numpy.savez(self.save_params_to, test_acc = test_acc, **best_params)

        # time taken
        total_time = end_time - start_time
        print('The code ran for %d epochs and took %d sec/epoch' % (epoch_count, \
              total_time/(epoch_count*1.0)))
        print('Total time taken: %d secs' % total_time, file=sys.stderr)
        return train_acc, valid_acc, test_acc

