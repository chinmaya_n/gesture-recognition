from __future__ import print_function
import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId

class MongoDb:
    """ MongoDB intergation class for the gesture recording """

    def __init__(self, dbstring='mongodb://localhost:27017', db_name='LMData', collection_name='Recordings'):
        """ Creates the MongoDb connection """
        # DB connection parameters
        self.client = MongoClient(dbstring)
        self.database = self.client[db_name]
        self.collection = self.database[collection_name]

    def insert(self, document):
        """ Inserts the 'document' into DB """
        return self.collection.insert_one(document).inserted_id

    def get_frame_timings(self, docId):
        """
        Get the frame timings for the recording id as a dictionary (key as frame id &
        value being the time stamp)
        """
        frameTimings = {}
        result = self.collection.find_one({"_id": ObjectId(docId)},
                                          {"frames.frame_id": 1, "frames.timestamp": 1})
        baseTime = result['frames'][0]['timestamp']
        for frame in result['frames']:
            frameTimings[frame['frame_id']] = frame['timestamp'] - baseTime
        return frameTimings

    def update_frame_info(self, docId, frameLabel, startId, endId, gestureType):
        """
        Update the frames in DB with frame label for given frame ranges
        """
        # update gesture_status for each frame
        for i in range(startId, endId+1):
            # check if the given frame exists, if not skip
            l = self.collection.find({"_id": ObjectId(docId), "frames.frame_id": i}).count()
            if l==0:
                continue

            r = self.collection.update({"_id": ObjectId(docId), "frames.frame_id": i},
                                       {"$set": {"frames.$.gesture_status": int(frameLabel),
                                                  "frames.$.gesture_type": int(gestureType)
                                                }
                                        })
            if r['updatedExisting']==False:
                print("Error: Could not update document- ", docId, frameLabel, i, gestureType)
                return False
        # successfully finished
        self.collection.update({"_id": ObjectId(docId)},
                               {"$set":{"annotation":"done"}})
        return True

    def get_index_finger_info(self, docId):
        """
        Get index finger velocity and frame info
        """
        result = self.collection.find_one({"_id":ObjectId(docId)},
                                          {"frames.index_tip_velocity":1,
                                           "frames.gesture_status":1})
        return result
