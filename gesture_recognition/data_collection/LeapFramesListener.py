######################################################################
# Leap Motion Listener for Data Collection
######################################################################
from __future__ import print_function
import Leap

class LeapFramesListener(Leap.Listener):
    """
    Listener Class for the Leap Motion Controller. Here we write the tasks to be
    done in each frame.
    """

    # Member Functions
    def on_init(self, controller):
        # Leap Initialization
        print("LeapMotion initialized")
        self.start_rec = False
        self.frame_id = 0
        self.frame_list = []

    def on_connect(self, controller):
        # LeapMotion Device connected
        print("LeapMotion Device Connected")

    def on_disconnect(self, controller):
        # LeapMotion Disconnected
        print("LeapMotion Device Disconnected")

    def on_exit(self, controller):
        # Program Exiting
        print("Exited the LeapMotion Controller!")

    def on_frame(self, controller):
        """Work to be done on this frame"""

        # save the required info from the frame if recording flag is set
        frame = controller.frame()
        if self.start_rec and frame.is_valid:   # and len(frame.fingers)>0
            # Add all the required Frame data
            frame_data = {}
            self.frame_id += 1
            frame_data.update(frame_id = self.frame_id)

            # Fingers
            fingers = frame.fingers
            
            # test for fingers present
            if len(fingers) != 5:
                return
            # only one hand is expected for the gestures for now
            if len(frame.hands)==1:
                pass
            else:
                print("More than one hand is observed. Only single hand gestures are premitted for now!")
                return
            
            # record each finger
            for finger in fingers:
                if(finger.type == 0):
                    frame_data.update(thumb = [finger.tip_position.x, finger.tip_position.y, finger.tip_position.z])
                    frame_data.update(thumb_tip_velocity = [finger.tip_velocity.x, finger.tip_velocity.y, finger.tip_velocity.z])
                    frame_data.update(thumb_is_valid = finger.is_valid)
                elif(finger.type == 1):
                    frame_data.update(index = [finger.tip_position.x, finger.tip_position.y, finger.tip_position.z])
                    frame_data.update(index_tip_velocity = [finger.tip_velocity.x, finger.tip_velocity.y, finger.tip_velocity.z])
                    frame_data.update(index_is_valid = finger.is_valid)
                elif(finger.type == 2):
                    frame_data.update(middle = [finger.tip_position.x, finger.tip_position.y, finger.tip_position.z])
                    frame_data.update(middle_tip_velocity = [finger.tip_velocity.x, finger.tip_velocity.y, finger.tip_velocity.z])
                    frame_data.update(middle_is_valid = finger.is_valid)
                elif(finger.type == 3):
                    frame_data.update(ring = [finger.tip_position.x, finger.tip_position.y, finger.tip_position.z])
                    frame_data.update(ring_tip_velocity = [finger.tip_velocity.x, finger.tip_velocity.y, finger.tip_velocity.z])
                    frame_data.update(ring_is_valid = finger.is_valid)
                elif(finger.type == 4):
                    frame_data.update(pinky = [finger.tip_position.x, finger.tip_position.y, finger.tip_position.z])
                    frame_data.update(pinky_tip_velocity = [finger.tip_velocity.x, finger.tip_velocity.y, finger.tip_velocity.z])
                    frame_data.update(pinky_is_valid = finger.is_valid)

            # Frame Timestamp
            frame_data.update(timestamp = frame.timestamp)

            # Gesture status in Frame
            frame_data.update(gesture_status = 0)

            # Gesture type
            frame_data.update(gesture_type = 0)

            # Save frame data
            self.frame_list.append(frame_data)
