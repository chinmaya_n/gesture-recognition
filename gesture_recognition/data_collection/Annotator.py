from __future__ import print_function
import numpy, math, os, glob
from MongoDb import MongoDb

class Annotator(object):
    """
    To annotate the recorded frames using videos. This helps to know when a
    gesture has happened. This enables the model to train on those patterns
    in data to learn & recognize it next time.
    """
    def __init__(self, video_files_location, video_format, db_connection_string, db_name, db_collection):
        # params
        self.video_files_location = video_files_location
        self.video_format = video_format
        self.db_connection_string = db_connection_string
        self.db_name = db_name
        self.db_collection = db_collection
        # start video annotation
        self.start_video_annotation()

    def video_annotator(self):
        """
        Annotate video to the give file from user inputs
        """
        # list of annotations
        annotations = []

        # Gesture start time (to subtract from rest of the timestamps)
        beginTime = raw_input("Enter the gesture recording begin time (h.m.s.msec): ")

        # Intro
        print("\nEnter 1 to report a gesture.")
        print("Enter 2 to report a transition gesture.")
        print("Enter q to quit & write annotation to the file.")
        print("Enter r to pop the last gesture reported.")
        print("Follow the instructions on screen after reporting.")

        # collect time stamps for each gesture
        key = raw_input("1/2/r/q >> ")
        while(key):
            # check the key
            if key=="q":
                # return annotations
                return annotations
            elif key=="1":
                startTime = raw_input("Enter the start time (h.m.s.msec): ")
                endTime = raw_input("Enter the end time (h.m.s.msec): ")
                gestureType = raw_input("Enter the gesture type. \n1-Circle \n2-Swipe \
                \n3-Correct \n4-Wrong \n5-Pull \n6-Push \nNo: ")
                # note annotation
                item = (int(key), (self.format_time(startTime, beginTime), self.format_time(endTime, beginTime)), int(gestureType))
                annotations.append(item)
            elif key=="2":
                startTime = raw_input("Enter the start time (h.m.s.msec): ")
                endTime = raw_input("Enter the end time (h.m.s.msec): ")
                # note annotation
                item = (int(key), (self.format_time(startTime, beginTime), self.format_time(endTime, beginTime)))
                annotations.append(item)
            elif key=="r":
                print("Deleting the last entry: ", annotations.pop())
            else:
                print("Enter correct input key (1/2/q/r) !\n")

            # read input
            key = raw_input("1/2/r/q >> ")

    def convert_to_microsec(self, time):
        """
        Convert hh.mm.ss.msec to micro seconds
        """
        (h,m,s,ms) = time.split(".")
        # find total seconds
        total_secs = int(h)*60*60 + int(m)*60 + int(s)
        # micro secs
        micro_secs = total_secs * math.pow(10, 6) + int(ms) * math.pow(10, 3)
        # return
        return int(micro_secs)

    def format_time(self, time, begin_time):
        """
        Adjust time to micro seconds and also substracting the begin_time from given time
        to get the absolute time.
        """
        return self.convert_to_microsec(time)-self.convert_to_microsec(begin_time)

    def update_frame_type(self, docId, annotations):
        """ """
        # get frame timing
        mdb = MongoDb(self.db_connection_string, self.db_name, self.db_collection)
        frame_timing = mdb.get_frame_timings(docId)

        # update status
        update_status = []
        # for each annotation update frame type in db
        for ann in annotations:
            if ann[0]==1:
                annType, (sTime, eTime), gType = ann
            else:
                annType, (sTime, eTime) = ann
                gType = 0
            # find the range of frame ids for the start and end times of annotation
            sId = 0
            eId = 0
            for i in range(len(frame_timing)):
                i += 1
                # check if frame exists, if not skip it
                if not frame_timing.get(i, False):
                    continue
                if sId==0:
                    if sTime <= frame_timing[i]:
                        sId = i
                elif eId==0:
                    if eTime <= frame_timing[i]:
                        eId = i
                if sId and eId: # found the frame ids for annotation
                    break
            # update the db for annotated frames
            if sId and eId:
                #print(sId, "-" ,eId)
                update_status.append(mdb.update_frame_info(docId, annType, sId, eId, gType))
        # check if all updates are done
        if (False in update_status) or (len(update_status)==0):
            return False
        # updates are successful
        return True

    def start_video_annotation(self):
        """
        Start the video annotation process by selecting a video file a display the name.
        Then user opens the video in some external video player and answers the questions here.
        Once the annotation is finished, db is updated and document is marked as
        annotation: "done" and video is moved to Finished directory in same video_files_location.

        video_files_location: /home/user/Videos/webcam/
        videoFormat : webm/mp4/mkv etc. [any video format for filtering in given directory]
        """
        # get all video files in given directory
        video_files_location = os.path.abspath(self.video_files_location)
        video_files = glob.glob(pathname=self.video_files_location+"/*."+self.video_format)
        mdb = MongoDb(self.db_connection_string, self.db_name, self.db_collection)
        # create Finished directory if not exists
        if not os.path.exists(video_files_location+"/Finished"):
            os.makedirs(video_files_location+"/Finished")
        # annotate for each video file
        for video_file in video_files:
            # ask if user wants to annotate video files
            forward = raw_input("Do you want to annotate a video file (y/n) >> ")
            if forward not in ["y", "Y", "yes", "Yes", "YES"]:
                print("Video Annotation Stopped as requested.")
                return
            # file name
            video_file_name = video_file.replace(video_files_location+"/", "")
            # Annotation
            print("Annotating for video %s has begun, please open it in your favorite video \
            player and continue with annotation." %(video_file_name))
            annotations = self.video_annotator()
            update_status = self.update_frame_type(docId=video_file_name[0:-len(self.video_format)-1], annotations=annotations)
            # update_status = True => annotation successful. So, move file finished.
            if update_status:
                print("Moving file ", video_file_name, " to ./Finished/ folder.")
                os.rename(video_file, video_files_location+"/Finished/"+video_file_name)
            else:
                print("Could not update annotations into DB for video file: ", video_file)
        # Finished Annotation
        print("Video Files annotation completed.")
