from __future__ import print_function
from MongoDb import MongoDb

class DBtoFile(object):
    """
    Class to create data files from MongoDB.
    """
    def __init__(self, db_connection_string, db_name, db_collection):
        """Init function for DBtoFile class."""
        self.mdb = MongoDb(dbstring=db_connection_string, db_name=db_name, collection_name=db_collection)
    
    def frames_data(self, frames_data_file, frames_labels_file, strict_gestures=True):
        """DBtoFile for frames data."""
        result = self.mdb.collection.find({"annotation":"done"})  # {"realtime":True}
        # open files
        frames_file = open(frames_data_file, 'w')
        frames_label_file = open(frames_labels_file, 'w')
        # write number of examples in the first line
        frames_file.write(str(result.count()) + '\n')
        frames_label_file.write(str(result.count()) + '\n')
        # for each document in the result: save to file
        for doc in result:
            # check if it is a valid doc - if not skip it
            if(len(doc['frames'])!=0):
                # text line for the gesture
                line = ''
                labels_line = ''
                # write finger positions and velocity to line
                for frame in doc['frames']:
                    temp_line = line
                    try:    # check for invalid frames and skip them i.e frames without any of the required keys
                        # fingers & palm positions
                        line += str(frame['thumb'][0]) + " " + str(frame['thumb'][1]) + " " + str(frame['thumb'][2]) + " "
                        line += str(frame['index'][0]) + " " + str(frame['index'][1]) + " " + str(frame['index'][2]) + " "
                        line += str(frame['middle'][0]) + " " + str(frame['middle'][1]) + " " + str(frame['middle'][2]) + " "
                        line += str(frame['ring'][0]) + " " + str(frame['ring'][1]) + " " + str(frame['ring'][2]) + " "
                        line += str(frame['pinky'][0]) + " " + str(frame['pinky'][1]) + " " + str(frame['pinky'][2]) + " "
                        # fingers & palm speeds
                        line += str(frame['thumb_tip_velocity'][0]) + " " + str(frame['thumb_tip_velocity'][1]) + " " + str(frame['thumb_tip_velocity'][2]) + " "
                        line += str(frame['index_tip_velocity'][0]) + " " + str(frame['index_tip_velocity'][1]) + " " + str(frame['index_tip_velocity'][2]) + " "
                        line += str(frame['middle_tip_velocity'][0]) + " " + str(frame['middle_tip_velocity'][1]) + " " + str(frame['middle_tip_velocity'][2]) + " "
                        line += str(frame['ring_tip_velocity'][0]) + " " + str(frame['ring_tip_velocity'][1]) + " " + str(frame['ring_tip_velocity'][2]) + " "
                        line += str(frame['pinky_tip_velocity'][0]) + " " + str(frame['pinky_tip_velocity'][1]) + " " + str(frame['pinky_tip_velocity'][2]) + " "
                        # frame label
                        label = int(frame['gesture_status'])
                        if strict_gestures:
                            gesture_labels = [1]    # include only real gestures, no transition gestures
                        else:
                            gesture_labels = [1, 2] # [1, 2] to include transition gestures
                        if label in gesture_labels: 
                            label = 1
                        else:
                            label = 0
                        labels_line += str(label) + " "
    
                    except KeyError as err:
                        print(err, " ", doc['_id'])
                        line = temp_line
    
                # write to file
                frames_file.write(line+ "\n")
                frames_label_file.write(labels_line+ "\n")
    
        # close the files
        frames_file.close()
        frames_label_file.close()        
    
    def gestures_data(self, gestures_file):
        """ DBtoFile Gestures data """
        # condition for the query
        condition = {'annotation':'done'}
        # query the database
        result = self.mdb.collection.find(condition)
        # gesture count
        gesture_count = 0
        # create a file to write the data to
        data_file = open(gestures_file, 'w')
        # operate over the result. Each document is a gesture.
        for doc in result:
            prevGestureStatus = -1  # old gesture status
            prevGestureLabel = -1
            gestureFound = False
            line = ''
            # iterate over each frame
            for frame in doc['frames']:
                # check for change in gesture status
                if prevGestureStatus != frame['gesture_status']:
                    # check if the frame has any gesture
                    if frame['gesture_status'] in [1, 2]:
                        gestureFound = True
                    else:
                        gestureFound = False
                    # save the recorded gesture to file
                    if prevGestureStatus in [1, 2]:
                        line = str(prevGestureLabel) + " " + line
                        data_file.write(line + '\n') # write line to file
                        gesture_count += 1
                        #print(prevGestureLabel)
                        line = ''
    
                # set previous gesture status
                prevGestureStatus = frame['gesture_status']
                prevGestureLabel = frame['gesture_type']
                # collect the frame data
                if gestureFound:
                    temp_line = line
                    try:    # check for invalid frames and skip them i.e frames without any of the required keys
                        # fingers & palm positions
                        line += str(frame['thumb'][0]) + " " + str(frame['thumb'][1]) + " " + str(frame['thumb'][2]) + " "
                        line += str(frame['index'][0]) + " " + str(frame['index'][1]) + " " + str(frame['index'][2]) + " "
                        line += str(frame['middle'][0]) + " " + str(frame['middle'][1]) + " " + str(frame['middle'][2]) + " "
                        line += str(frame['ring'][0]) + " " + str(frame['ring'][1]) + " " + str(frame['ring'][2]) + " "
                        line += str(frame['pinky'][0]) + " " + str(frame['pinky'][1]) + " " + str(frame['pinky'][2]) + " "
                        # fingers & palm speeds
                        line += str(frame['thumb_tip_velocity'][0]) + " " + str(frame['thumb_tip_velocity'][1]) + " " + str(frame['thumb_tip_velocity'][2]) + " "
                        line += str(frame['index_tip_velocity'][0]) + " " + str(frame['index_tip_velocity'][1]) + " " + str(frame['index_tip_velocity'][2]) + " "
                        line += str(frame['middle_tip_velocity'][0]) + " " + str(frame['middle_tip_velocity'][1]) + " " + str(frame['middle_tip_velocity'][2]) + " "
                        line += str(frame['ring_tip_velocity'][0]) + " " + str(frame['ring_tip_velocity'][1]) + " " + str(frame['ring_tip_velocity'][2]) + " "
                        line += str(frame['pinky_tip_velocity'][0]) + " " + str(frame['pinky_tip_velocity'][1]) + " " + str(frame['pinky_tip_velocity'][2]) + " "
                    except KeyError as err:
                        print(err)
                        line = temp_line
        # close the file
        data_file.close()
        # add first line as number of gestures found
        with open(gestures_file, "r+") as gfile:
            # code from here: http://stackoverflow.com/a/4454490/815539
            data = gfile.read()
            gfile.seek(0)
            gfile.write(str(gesture_count)+"\n"+data)
