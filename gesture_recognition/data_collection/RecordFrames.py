from __future__ import print_function
import Leap, sys, readchar
import glob, os

from LeapFramesListener import LeapFramesListener
from MongoDb import MongoDb

def RecordFrames(mdb_connection_string, mdb_name, mdb_collection_name, 
                 video_record_pattern, video_move_loc, video_format):
    """
    Start recording the stream of frames while drawing gestures to capture them.
    """
    # Leap Motion initialization
    controller = Leap.Controller()
    listener = LeapFramesListener()
    controller.add_listener(listener)
    db = MongoDb(dbstring=mdb_connection_string, db_name=mdb_name, collection_name=mdb_collection_name)

    # Record gesture stream
    try:
        print("NOTE: This function (RecordFrames) uses `readchar` package, so, it must be"
              " run from a terminal to work properly!")
        print("NOTE: Prepare to record a video of the recording to annotate later.\n")
        print("Press 'q' to start recording frames and 'w' to stop.")
        print("Press 'z' to stop the program completely.")

        # iterate until program is quit
        while True:
            # read a character
            print(">> ", end="")
            key = readchar.readkey()
            print(key)

            # Take action based on the key
            if key=='q':
                # start stream recording
                print("You should have started recording the video for annotation by now.")
                print("Started recording frames....")
                listener.frame_list = []    # reset the frame list
                listener.frame_id = 0       # reset frame id
                listener.start_rec = True   # start recording frames
            elif key=='w':
                # stop stream recording
                listener.start_rec = False
                print("Stop recording frames....")
                frame_list = listener.frame_list
                # save the gestures
                if len(frame_list) != 0:
                    doc_id = db.insert({"frames": frame_list})
                    print("Doc Id: %s & No. of frames: %d." %(doc_id, len(frame_list)))
                    # clear frame list
                    listener.frame_list = []
                    
                    # Wait to save the video
                    ans = raw_input("Is the video recording stopped? (y/n): ")
                    if ans in ["y", "yes", "Y", "YES", "Yes"]:
                        files = glob.glob(video_record_pattern)
                        if len(files)==1:
                            print("Moved the video to LeapRecording and renamed.")
                            os.rename(files[0], os.path.abspath(video_move_loc)+"/"+str(doc_id)+ "."+ video_format)
                        else:
                            print("More than one file found in given folder. Save it manually with name: ", doc_id)
                    else:
                        print("Please save video manually & rename it as: ", doc_id)
                    
                else:
                    print("Frame List is empty!")
            elif key=='z':
                # quit the program
                print("Quiting the program....")
                break
            else:
                print("Not a correct option!")
                print("Press 'q' to start recording gestures and 'w' to stop.")
                print("Press 'z' to stop the program.")

    except KeyboardInterrupt:
        pass
    except Exception, e:
        print("Exception: ", e)
    finally:
        controller.remove_listener(listener)
