class Config(object):
    # =========================================
    # Model Configuration for Gesture Detection
    # =========================================
    
    # general parameters
    frame_size = 30
    memory_size = frame_size
    optimizer = "adadelta" #GestureDetection.adadelta
    regularization_lambda = 0.0
    log_offset = 1e-6
    number_of_frame_types = 2
    learning_rate = 0.01
    patience = 10
    label_slide_width = 15
    maximum_epochs = 50
    log_display_frequency = 3000
    validation_frequency = 50000
    params_save_frequency = 50000
    
    # file names
    parent_path = "/home/inblueswithu/Documents/gesture_recognition/gesture_recognition/example/experiments/"
    identity_name = "run1"
    save_params_to = parent_path + "/data/GD_params_" + identity_name + ".npz"
    #frames_data_file = parent_path + "/data/GD_FramesData.txt"
    #frames_labels_file = parent_path + "/data/GD_FramesLabels_no2.txt"
    
    gd_train_frames_file = './data/GD_FramesData_Train.txt'
    gd_train_labels_file = './data/GD_FramesLabels_Train.txt'
    gd_valid_frames_file = './data/GD_FramesData_Valid.txt'
    gd_valid_labels_file = './data/GD_FramesLabels_Valid.txt'
    gd_test_frames_file = './data/GD_FramesData_Test.txt'
    gd_test_labels_file = './data/GD_FramesLabels_Test.txt'
    
    load_params_from = parent_path + "/data/GD_params_" + identity_name + ".npz"
    
    # ==============================================
    # Model Configuration for Gesture Classification
    # ==============================================
    
    # general parameters
    gc_memory_size = frame_size
    gc_regularization_lambda = 0.0
    gc_log_offset = 1e-6
    number_of_gesture_types = 7
    gc_learning_rate = 0.01
    gc_patience = 5
    gc_maximum_epochs = 100
    gc_log_display_frequency = 50
    gc_validation_frequency = 1200
    gc_params_save_frequency = 2000
    
    # file names
    gc_train_gestures_file = './data/GC_GesturesData_Train.txt'
    gc_valid_gestures_file = './data/GC_GesturesData_Valid.txt'
    gc_test_gestures_file = './data/GC_GesturesData_Test.txt'
    #gestures_data_file = parent_path + "/data/GC_GesturesData.txt"
    gc_save_params_to = parent_path + "/data/GC_params_" + identity_name + ".npz"
    gc_load_params_from = parent_path + "/data/GC_params_" + identity_name + ".npz"
    
    # ============================
    # Frames Handling options
    # ============================
    
    frame_labels_patience = 10
    boundary_inclusion = 10
    gesture_frames_count_threshold = 30
