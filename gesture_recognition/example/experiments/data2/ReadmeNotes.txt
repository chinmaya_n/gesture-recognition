NOTE:
    
    WSTestGestureRegionsFold <k> .txt are the files with gesture regions. They are used
    to send the gesture regions from our LSTM GD module to AE+CNN module. Once we get the
    predicted labels for these gesture regions from AE+CNN module, we will compute the
    Whole System (WS) performance.
