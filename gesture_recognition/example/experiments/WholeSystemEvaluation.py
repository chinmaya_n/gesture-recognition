from __future__ import print_function
import numpy
from gesture_recognition.mlcore.GestureClassificationPredict import GestureClassificationPredict
from config import Config as config

def get_gesture_regions(label_list):
    """
    Get the list of gesture regions in the given frames label list
    """
    gesture_regions = []
    begin = 0
    end = 0
    region_found = False
    total_len = len(label_list)
    for i in range(total_len):
        label = numpy.int(label_list[i])
        if label == 1 and region_found == False:
            # region started
            region_found = True
            begin = i
        elif label == 0 and region_found == True:
            region_found = False
            end = i
            # region ended. update the list
            if end > begin and (begin, end) not in gesture_regions:
                gesture_regions.append((begin, end))
        elif label == 1 and region_found == True:
            # gesture region is in the end of the label list
            if i+1 == total_len:
                region_found = False
                end = i
                gesture_regions.append((begin, end))
    # return
    return gesture_regions

def find_matching_regions(true_labels_file, pred_labels_file):
    """
    Find the matching regions between the true and predicted labels
    """
    #gesture_labels = numpy.load(gesture_labels_file)
    labels = numpy.load(true_labels_file)
    true_gesture_regions = get_gesture_regions(labels["trueLabels"])
    labels = numpy.load(pred_labels_file)
    pred_gesture_regions = get_gesture_regions(labels["predLabels"])
    matching_index = []
    # check matching
    for pi in range(len(pred_gesture_regions)):
        pred_region = pred_gesture_regions[pi]
        match = -1
        for ti in range(len(true_gesture_regions)):
            true_region = true_gesture_regions[ti]
            if pred_region[0] < true_region[1] and pred_region[1] > true_region[0]:
                true_region_length = true_region[1] - true_region[0]
                # to get matching region length
                s = sorted([pred_region[0], pred_region[1], true_region[0], true_region[1]])
                matching_region_length = s[2]-s[1]
                if true_region_length/2.0 <= matching_region_length:
                    match = ti
                    break
        matching_index.append(match)
    # return
    return true_gesture_regions, pred_gesture_regions, matching_index

def get_data_from_file(frames_file, frame_length):
    """
    Gets the data from the file.
    """
    # frames file
    fp = open(frames_file, 'r')
    example_count = int(fp.readline())
    data = []
    for line in fp.readlines():
        line = [float(num) for num in line.split()]
        example = numpy.array(line[0:]).reshape((len(line))/frame_length, frame_length)
        data.append(example.transpose())
    fp.close()
    # data
    return data

def get_all_test_frames(test_frames_file):
    """
    Get all the test frames concatinated to match the true & predicted label indexing.
    """
    all_test_egs = get_data_from_file(frames_file=test_frames_file, frame_length=30)
    all_test_frames = numpy.zeros((30,1))
    for eg in all_test_egs:
        all_test_frames = numpy.concatenate((all_test_frames, eg), axis=1)
    return all_test_frames[:,1:]

def calculate_PRF(location):
    """
    Calculate Precision-Recall-F Measure for the Whole system.
    """
    pn = 0.0  # precision calculation numerator
    pd = 0.0  # precision calculation denominator
    rn = 0.0  # recall calculation numerator
    rd = 0.0  # recall calculation denominator
    # for each fold
    for f in range(10)[1:]:
        data = numpy.load(location+"WSLabelsFold"+str(f)+".npz")
        true_labels = data["trueLabels"].tolist()
        pred_labels = data["predLabels"].tolist()
        matching_index = data["matchingIndex"].tolist()
        # calculate precision
        pd += len(pred_labels)
        for i in range(len(matching_index)):
            match_id = matching_index[i]
            if match_id != -1:
                if pred_labels[i]+1==true_labels[match_id]:
                    pn += 1
            else:
                if pred_labels[i] == 0:
                    pn += 1
        # calculate recall
        rd += len(true_labels)-true_labels.count(1)
        for i in range(len(true_labels)):
            try:
                pos = matching_index.index(i)
            except ValueError:
                continue
            if pred_labels[pos]+1==true_labels[i]:
                rn += 1
    # return
    precision = pn/pd
    recall = rn/rd
    f_measure = (2*precision*recall)/(precision+recall)
    return precision, recall, f_measure

def calculate_PRF_AECNN(location):
    """
    Calculate Precision-Recall-F Measure for the Whole system.
    """
    pn = 0.0  # precision calculation numerator
    pd = 0.0  # precision calculation denominator
    rn = 0.0  # recall calculation numerator
    rd = 0.0  # recall calculation denominator
    # for each fold
    for f in range(10)[1:]:
        data = numpy.load(location+"WS_AE_CNN_LabelsFold"+str(f)+".npz")
        true_labels = data["trueLabels"].tolist()
        pred_labels = data["predLabels"].tolist()
        matching_index = data["matchingIndex"].tolist()
        # calculate precision
        pd += len(pred_labels)
        for i in range(len(matching_index)):
            match_id = matching_index[i]
            if match_id != -1:
                if pred_labels[i]+1==true_labels[match_id]:
                    pn += 1
            else:
                if pred_labels[i] == 0:
                    pn += 1
        # calculate recall
        rd += len(true_labels)-true_labels.count(1)
        for i in range(len(true_labels)):
            try:
                pos = matching_index.index(i)
            except ValueError:
                continue
            if pred_labels[pos]+1==true_labels[i]:
                rn += 1
    # return
    precision = pn/pd
    recall = rn/rd
    f_measure = (2*precision*recall)/(precision+recall)
    return precision, recall, f_measure

def check_boundary_match_LSTM(location):
    """ Check the boundary match between the gesture regions for true and predicted. """
    for f in range(10)[1:]:
        # get the gesture regions and matchings
        true_gesture_regions, pred_gesture_regions, matching_index = find_matching_regions(
                                    true_labels_file = "./data3/LabelsFold"+str(f)+".npz",          # True Labels, so, it is ok to take from data3. They are the same.
                                    pred_labels_file = location+"PostProcLabelsFold"+str(f)+".npz") #"./data2/PostProcLabelsFold1.npz")

        # get the test frames
        test_frames = get_all_test_frames(location+"Fold"+str(f)+"FramesTest.txt") #'./data2/Fold1FramesTest.txt')

        # get pred labels for matching regions
        config.gc_save_params_to = location+"GC_Params_Fold"+str(f)+".npz" #"./data2/GC_Params_Fold1.npz"
        config.gc_load_params_from = config.gc_save_params_to
        gcp = GestureClassificationPredict(config, config.gc_load_params_from)
        pred_labels = []
        for i in range(len(pred_gesture_regions)):
            gesture_region = pred_gesture_regions[i]
            pred_labels.extend(gcp.predict_label(test_frames[:,gesture_region[0] : gesture_region[1]]).tolist())

        # true labels for the regions in true gesture regions
        true_labels = numpy.load(location+"GestureLabelsFold"+str(f)+".npz")['trueLabels'] #"./data2/GestureLabelsFold1.npz")['trueLabels']

        # check performance
        right = 0
        wrong = 0
        for i in range(len(matching_index)):
            match_id = matching_index[i]
            if match_id != -1:
                # check the boundary matching percentage
                tbf = [true_gesture_regions[match_id][j] for j in [0,1]]
                pbf = [pred_gesture_regions[i][j] for j in [0,1]]
                sbf = sorted([tbf[0], tbf[1], pbf[0], pbf[1]]) # sorted_boundary_frames 
                percent_of_matching = ((sbf[2]-sbf[1])*100.0)/(tbf[1]-tbf[0])
                # print the matching result
                info_string = str(percent_of_matching) + " - "
                if pred_labels[i]+1==true_labels[match_id]:
                    right += 1
                    info_string += "1 - " + str(pred_labels[i]+1) + " - " + str(true_labels[match_id])
                else:
                    wrong += 1
                    info_string += "0 - " + str(pred_labels[i]+1) + " - " + str(true_labels[match_id])
                print(info_string)
        #print(right, wrong)
    
def check_boundary_match_AECNN(location):
    """ Check the boundary match between the gesture regions for true and predicted. """
    for f in range(10)[1:]:
        # get the gesture regions and matchings
        true_gesture_regions, pred_gesture_regions, matching_index = find_matching_regions(
                                    true_labels_file = "./data3/LabelsFold"+str(f)+".npz",
                                    pred_labels_file = location+"PostProcLabelsFold"+str(f)+".npz") #"./data2/PostProcLabelsFold1.npz")
        
        # get the true and pred labels
        data = numpy.load(location+"WS_AE_CNN_LabelsFold"+str(f)+".npz")
        true_labels = data["trueLabels"].tolist()
        pred_labels = data["predLabels"].tolist()
        matching_index_2 = data["matchingIndex"].tolist()      

        # check if matching indexes are same
        #if matching_index == matching_index_2:
            #print("Matching indexes Same.")
        #else:
            #print("Matching indexes are not same. Please check your data files.")
            #return
        
        # check performance
        right = 0
        wrong = 0
        for i in range(len(matching_index)):
            match_id = matching_index[i]
            if match_id != -1:
                # check the boundary matching percentage
                tbf = [true_gesture_regions[match_id][j] for j in [0,1]]
                pbf = [pred_gesture_regions[i][j] for j in [0,1]]
                sbf = sorted([tbf[0], tbf[1], pbf[0], pbf[1]]) # sorted_boundary_frames 
                percent_of_matching = ((sbf[2]-sbf[1])*100.0)/(tbf[1]-tbf[0])
                # print the matching result
                info_string = str(round(percent_of_matching,2)) + " - "
                if pred_labels[i]+1==true_labels[match_id]:
                    right += 1
                    info_string += "1 - " + str(pred_labels[i]+1) + " - " + str(true_labels[match_id])
                else:
                    wrong += 1
                    info_string += "0 - " + str(pred_labels[i]+1) + " - " + str(true_labels[match_id])
                print(info_string)
        #print(right, wrong)

def save_test_gesture_regions(location):
    """ To send the test gesture regions to AE+CNN model """
    for f in range(10)[1:]:
        # get the gesture regions and matchings
        true_gesture_regions, pred_gesture_regions, matching_index = find_matching_regions(
                                    true_labels_file = "./data3/LabelsFold"+str(f)+".npz",
                                    pred_labels_file = location+"PostProcLabelsFold"+str(f)+".npz") #"./data2/PostProcLabelsFold1.npz")

        # get the test frames
        test_frames = get_all_test_frames(location+"Fold"+str(f)+"FramesTest.txt") #'./data2/Fold1FramesTest.txt')

        # save the gesture regions into a file
        g_file = open(location+'WSTestGestureRegionsFold'+str(f)+'.txt', mode='w')
        g_file.write(str(len(pred_gesture_regions))+"\n")
        for i in range(len(pred_gesture_regions)):
            gesture_region = pred_gesture_regions[i]
            gesture_frames = test_frames[:,gesture_region[0] : gesture_region[1]]
            g_file.write('10 ')  # dummy label for the gesture - we do not know the label yet.
            for c in range(gesture_frames.shape[1]):
                for r in range(gesture_frames.shape[0]):
                    g_file.write(str(gesture_frames[r,c])+ " ")
            g_file.write('\n')  # end of gesture.


def main(location):
    """
    For Evaluating the whole system with GC. i.e
    > GD + GC as a complete system.
    """
    for f in range(10)[1:]:
        # get the gesture regions and matchings
        true_gesture_regions, pred_gesture_regions, matching_index = find_matching_regions(
                                    true_labels_file = "./data3/LabelsFold"+str(f)+".npz",          # True Labels, so, it is ok to take from data3. They are the same.
                                    pred_labels_file = location+"PostProcLabelsFold"+str(f)+".npz") #"./data2/PostProcLabelsFold1.npz")

        # get the test frames
        test_frames = get_all_test_frames(location+"Fold"+str(f)+"FramesTest.txt") #'./data2/Fold1FramesTest.txt')

        # get pred labels for matching regions
        config.gc_save_params_to = location+"GC_Params_Fold"+str(f)+".npz" #"./data2/GC_Params_Fold1.npz"
        config.gc_load_params_from = config.gc_save_params_to
        gcp = GestureClassificationPredict(config, config.gc_load_params_from)
        pred_labels = []
        for i in range(len(pred_gesture_regions)):
            gesture_region = pred_gesture_regions[i]
            pred_labels.extend(gcp.predict_label(test_frames[:,gesture_region[0] : gesture_region[1]]).tolist())

        # true labels for the regions in true gesture regions
        true_labels = numpy.load(location+"GestureLabelsFold"+str(f)+".npz")['trueLabels'] #"./data2/GestureLabelsFold1.npz")['trueLabels']

        # check performance
        right = 0
        wrong = 0
        for i in range(len(matching_index)):
            match_id = matching_index[i]
            if match_id != -1:
                if pred_labels[i]+1==true_labels[match_id]:
                    right += 1
                else:
                    wrong += 1
        print(right, wrong)

        # save the values # './data2/WSLabelsFold1.npz'
        numpy.savez(location+"WSLabelsFold"+str(f)+".npz", trueLabels=true_labels, predLabels=pred_labels, matchingIndex=matching_index)


def main2(location):
    """
    For Evaluating the whole system with AE+CNN. i.e
    > GD + (AE+CNN) as a complete system.
    """
    for f in range(10)[1:]:
        # get the gesture regions and matchings
        true_gesture_regions, pred_gesture_regions, matching_index = find_matching_regions(
                                    true_labels_file = "./data3/LabelsFold"+str(f)+".npz",          # True Labels, so, it is ok to take from data3. They are the same.
                                    pred_labels_file = location+"PostProcLabelsFold"+str(f)+".npz") #"./data2/PostProcLabelsFold1.npz")

        # get the test frames
        test_frames = get_all_test_frames(location+"Fold"+str(f)+"FramesTest.txt") #'./data2/Fold1FramesTest.txt')

        # get pred labels for matching regions
        pred_labels = []
        with open('./data2/WSTestGestureRegionsFold'+str(f)+'_AE_CNN_PredLabels.txt') as plFile:
            labels = plFile.readlines()
            for label in labels:
                pred_labels.append(int(label))

        # true labels for the regions in true gesture regions
        true_labels = numpy.load(location+"GestureLabelsFold"+str(f)+".npz")['trueLabels'] #"./data2/GestureLabelsFold1.npz")['trueLabels']

        # check performance
        right = 0
        wrong = 0
        for i in range(len(matching_index)):
            match_id = matching_index[i]
            if match_id != -1:
                if pred_labels[i]+1==true_labels[match_id]:
                    right += 1
                else:
                    wrong += 1
        print(right, wrong)

        # save the values # './data2/WS_AE_CNN_LabelsFold1.npz'
        numpy.savez(location+"WS_AE_CNN_LabelsFold"+str(f)+".npz", trueLabels=true_labels, predLabels=pred_labels, matchingIndex=matching_index)


if __name__ == "__main__":
    # #1
    #label_list = [0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1]
    #print(get_gesture_regions(label_list))

    # #2
    #find_matching_regions("./data3/LabelsFold1.npz", "./data2/PostProcLabelsFold1.npz")

    # #3
    #get_all_test_frames('./data2/Fold1FramesTest.txt')

    # #4
    #main("./data2/")

    # #5
    #print(calculate_PRF("./data2/"))

    # #3.1 - For AE+CNN
    #save_test_gesture_regions('./data2/')

    # #4.1 For AE+CNN
    #main2("./data2/")

    # #5.1 For AE+CNN
    #print(calculate_PRF_AECNN("./data2/"))
    
    # 6 Debug
    check_boundary_match_AECNN("./data2/")
