./data2/ directory
    Data files with 10 folds and *Transition gestures are ignored in Gesture Detection (GD)*.
    Folds are from same set of recordings for both Gesture Detection & Classification
    data files.

./data/ directory
    Data files with 10 folds and Transition gestures are ignored in Gesture Detection.
    Unlike ./data2, it is from different set of recordings. Created from data files already
    generated (from DBtoFile in data_collection).

./data3/ directory
    Data files with 10 folds and *Transition gestures are considered as gestures in GD*.
    Folds are from same set of recordings for both Gesture Detection & Classification
    data files.
