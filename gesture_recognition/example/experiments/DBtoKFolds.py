from __future__ import print_function
from sklearn.model_selection import KFold
from pymongo import MongoClient
import numpy

class DBtoKFolds(object):
    """
    DB to K folds where each fold in Gesture Detection and Classification are from
    same recordings. This helps to make sure the testing examples are not seen at
    any point of time in training.
    """
    def __init__(self, dbConnectionString, dbName, dbCollection, strictGestures, k, dataFileSaveLoc):
        """Init function for DBtoKFolds."""
        self.client = MongoClient(dbConnectionString)
        self.mdbCollection = self.client[dbName][dbCollection]
        self.k = k
        self.saveLoc = dataFileSaveLoc
        self.strictGestures = strictGestures
        # generate k fold files
        self.generateKFoldFiles()
        return

    def generateKFoldFiles(self):
        """Generate K Fold files"""
        result = self.mdbCollection.find({"annotation":"done"})
        noOfRecordings = result.count()
        foldSize = int(round(noOfRecordings/(1.0*self.k)))
        trainTestSize = noOfRecordings-foldSize
        validationIndex = numpy.array(range(foldSize))+trainTestSize
        kf = KFold(n_splits=self.k-1)
        foldNo = 0
        # for each fold
        for trainIndex, testIndex in kf.split(X=numpy.array(range(trainTestSize))):
            # count the fold
            foldNo += 1
            # open fold files for recordings
            fpFoldTrainFramesFile = open(self.saveLoc+"Fold"+str(foldNo)+"FramesTrain.txt", mode='w')
            fpFoldTestFramesFile = open(self.saveLoc+"Fold"+str(foldNo)+"FramesTest.txt", mode='w')
            fpFoldTrainFramesLabelsFile = open(self.saveLoc+"Fold"+str(foldNo)+"FramesLabelsTrain.txt", mode='w')
            fpFoldTestFramesLabelsFile = open(self.saveLoc+"Fold"+str(foldNo)+"FramesLabelsTest.txt", mode='w')
            if foldNo == 1:
                fpFoldValidFramesLabelsFile = open(self.saveLoc+"Fold"+"FramesLabelsValid.txt", mode='w')
                fpFoldValidFramesFile = open(self.saveLoc+"Fold"+"FramesValid.txt", mode='w')
            
            # write the first line for each file
            fpFoldTrainFramesFile.write(str(len(trainIndex))+"\n")
            fpFoldTestFramesFile.write(str(len(testIndex))+"\n")
            fpFoldTrainFramesLabelsFile.write(str(len(trainIndex))+"\n")
            fpFoldTestFramesLabelsFile.write(str(len(testIndex))+"\n")
            if foldNo == 1:
                fpFoldValidFramesFile.write(str(len(validationIndex))+"\n")
                fpFoldValidFramesLabelsFile.write(str(len(validationIndex))+"\n")

            # open fold files for gestues
            fpFoldGesturesTrainFile = open(self.saveLoc+"Fold"+str(foldNo)+"GesturesTrain.txt", mode='w')
            fpFoldGesturesTestFile = open(self.saveLoc+"Fold"+str(foldNo)+"GesturesTest.txt", mode='w')
            if foldNo==1:
                fpFoldGesturesValidFile = open(self.saveLoc+"Fold"+"GesturesValid.txt", mode='w')            
            
            # gestures count
            gesturesTrainCount = 0
            gesturesTestCount = 0
            gesturesValidCount = 0
            
            # write each example to files
            for i in range(noOfRecordings):
                doc = result[i] # document in the result
                if(len(doc['frames'])==0):  # test the document if it is empty
                    continue
                framesLine, framesLabelsLine = self.getFramesLines(doc)
                gesturesList = self.getGesturesLines(doc)
                gestureLines = ""
                for line in gesturesList:
                    gestureLines += line + "\n"
                # save in appropriate file
                if i in trainIndex:
                    fpFoldTrainFramesFile.write(framesLine+"\n")
                    fpFoldTrainFramesLabelsFile.write(framesLabelsLine+"\n")
                    fpFoldGesturesTrainFile.write(gestureLines)
                    gesturesTrainCount += len(gesturesList)
                elif i in testIndex:
                    fpFoldTestFramesFile.write(framesLine+"\n")
                    fpFoldTestFramesLabelsFile.write(framesLabelsLine+"\n")
                    fpFoldGesturesTestFile.write(gestureLines)
                    gesturesTestCount += len(gesturesList)
                elif i in validationIndex:
                    if foldNo == 1:
                        fpFoldValidFramesFile.write(framesLine+"\n")
                        fpFoldValidFramesLabelsFile.write(framesLabelsLine+"\n")
                        fpFoldGesturesValidFile.write(gestureLines)
                        gesturesValidCount += len(gesturesList)
            
            # close all file pointers
            fpFoldTrainFramesFile.close()
            fpFoldTestFramesFile.close()
            fpFoldTrainFramesLabelsFile.close()
            fpFoldTestFramesLabelsFile.close()
            if foldNo == 1:
                fpFoldValidFramesFile.close()
                fpFoldValidFramesLabelsFile.close()
        
            # close all the file pointers
            fpFoldGesturesTrainFile.close()
            fpFoldGesturesTestFile.close()
            if foldNo == 1:
                fpFoldGesturesValidFile.close()
            
            # add first line as number of gestures found
            with open(self.saveLoc+"Fold"+str(foldNo)+"GesturesTrain.txt", "r+") as gfile:
                # code from here: http://stackoverflow.com/a/4454490/815539
                data = gfile.read()
                gfile.seek(0)
                gfile.write(str(gesturesTrainCount)+"\n"+data)
            with open(self.saveLoc+"Fold"+str(foldNo)+"GesturesTest.txt", "r+") as gfile:
                data = gfile.read()
                gfile.seek(0)
                gfile.write(str(gesturesTestCount)+"\n"+data)
            if foldNo == 1:
                with open(self.saveLoc+"Fold"+"GesturesValid.txt", "r+") as gfile:
                    data = gfile.read()
                    gfile.seek(0)
                    gfile.write(str(gesturesValidCount)+"\n"+data)                
        
        return
    
    def getFramesLines(self, doc):
        """Get the frames & labels lines to save in file for the given document."""
        # text line for the gesture
        line = ''
        labelsLine = ''
        # write finger positions and velocity to line
        for frame in doc['frames']:
            tempLine = line
            try:    # check for invalid frames and skip them i.e frames without any of the required keys
                # fingers & palm positions
                line += str(frame['thumb'][0]) + " " + str(frame['thumb'][1]) + " " + str(frame['thumb'][2]) + " "
                line += str(frame['index'][0]) + " " + str(frame['index'][1]) + " " + str(frame['index'][2]) + " "
                line += str(frame['middle'][0]) + " " + str(frame['middle'][1]) + " " + str(frame['middle'][2]) + " "
                line += str(frame['ring'][0]) + " " + str(frame['ring'][1]) + " " + str(frame['ring'][2]) + " "
                line += str(frame['pinky'][0]) + " " + str(frame['pinky'][1]) + " " + str(frame['pinky'][2]) + " "
                # fingers & palm speeds
                line += str(frame['thumb_tip_velocity'][0]) + " " + str(frame['thumb_tip_velocity'][1]) + " " + str(frame['thumb_tip_velocity'][2]) + " "
                line += str(frame['index_tip_velocity'][0]) + " " + str(frame['index_tip_velocity'][1]) + " " + str(frame['index_tip_velocity'][2]) + " "
                line += str(frame['middle_tip_velocity'][0]) + " " + str(frame['middle_tip_velocity'][1]) + " " + str(frame['middle_tip_velocity'][2]) + " "
                line += str(frame['ring_tip_velocity'][0]) + " " + str(frame['ring_tip_velocity'][1]) + " " + str(frame['ring_tip_velocity'][2]) + " "
                line += str(frame['pinky_tip_velocity'][0]) + " " + str(frame['pinky_tip_velocity'][1]) + " " + str(frame['pinky_tip_velocity'][2]) + " "
                # frame label
                label = int(frame['gesture_status'])
                if self.strictGestures:
                    gestureLabels = [1]    # include only real gestures, no transition gestures
                else:
                    gestureLabels = [1, 2] # [1, 2] to include transition gestures
                if label in gestureLabels: 
                    label = 1
                else:
                    label = 0
                labelsLine += str(label) + " "
    
            except KeyError as err:
                print(err, " ", doc['_id'])
                line = tempLine
    
        # return the lines
        return (line, labelsLine)
    
    def getGesturesLines(self, doc):
        """Get the gestures from the frames as lines to write to file."""
        prevGestureStatus = -1  # old gesture status
        prevGestureLabel = -1
        gestureFound = False
        gestures = []   # gestures line list
        line = ''
        # iterate over each frame
        for frame in doc['frames']:
            # check for change in gesture status
            if prevGestureStatus != frame['gesture_status']:
                # check if the frame has any gesture
                if frame['gesture_status'] in [1, 2]:
                    gestureFound = True
                else:
                    gestureFound = False
                # save the recorded gesture to file
                if prevGestureStatus in [1, 2]:
                    line = str(prevGestureLabel) + " " + line
                    gestures.append(line) # append to gestures
                    line = ''   # reset the line
    
            # set previous gesture status
            prevGestureStatus = frame['gesture_status']
            prevGestureLabel = frame['gesture_type']
            # collect the frame data
            if gestureFound:
                temp_line = line
                try:    # check for invalid frames and skip them i.e frames without any of the required keys
                    # fingers & palm positions
                    line += str(frame['thumb'][0]) + " " + str(frame['thumb'][1]) + " " + str(frame['thumb'][2]) + " "
                    line += str(frame['index'][0]) + " " + str(frame['index'][1]) + " " + str(frame['index'][2]) + " "
                    line += str(frame['middle'][0]) + " " + str(frame['middle'][1]) + " " + str(frame['middle'][2]) + " "
                    line += str(frame['ring'][0]) + " " + str(frame['ring'][1]) + " " + str(frame['ring'][2]) + " "
                    line += str(frame['pinky'][0]) + " " + str(frame['pinky'][1]) + " " + str(frame['pinky'][2]) + " "
                    # fingers & palm speeds
                    line += str(frame['thumb_tip_velocity'][0]) + " " + str(frame['thumb_tip_velocity'][1]) + " " + str(frame['thumb_tip_velocity'][2]) + " "
                    line += str(frame['index_tip_velocity'][0]) + " " + str(frame['index_tip_velocity'][1]) + " " + str(frame['index_tip_velocity'][2]) + " "
                    line += str(frame['middle_tip_velocity'][0]) + " " + str(frame['middle_tip_velocity'][1]) + " " + str(frame['middle_tip_velocity'][2]) + " "
                    line += str(frame['ring_tip_velocity'][0]) + " " + str(frame['ring_tip_velocity'][1]) + " " + str(frame['ring_tip_velocity'][2]) + " "
                    line += str(frame['pinky_tip_velocity'][0]) + " " + str(frame['pinky_tip_velocity'][1]) + " " + str(frame['pinky_tip_velocity'][2]) + " "
                except KeyError as err:
                    print(err)
                    line = temp_line
        # return the gesture list
        return gestures

if __name__ == "__main__":
    d = DBtoKFolds(dbConnectionString="mongodb://localhost:27017/", 
                   dbName="frames_data", 
                   dbCollection="mock_training", 
                   strictGestures=False, k=10, 
                   dataFileSaveLoc="/home/inblueswithu/Documents/gesture_recognition/gesture_recognition/example/experiments/data3/")
