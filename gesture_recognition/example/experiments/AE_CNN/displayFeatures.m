%% Display Learned Features in Sparse Auto Encoder

function features = displayFeatures(W)
% Display the inputs that maximally activates the hidden neurons = learned
% features.
% Parameters: W : W1 (Learned weights from input to hidden layer)
% Output: Inputs that maximally activates hidden units

frameSize = 15;
[hiddenSize, visibleSize] = size(W);
dispSize = 3; %ceil(sqrt(hiddenSize));
features = zeros(visibleSize, hiddenSize);
% calculate the features
for i = 1:hiddenSize
    features(:,i) = reshape(W(i,:)./norm(W(i,:)), [], 1);
end

% visualize features
figure;
plotNo = 1;
for j = randi([1 hiddenSize], 1, dispSize^2) % hiddenSize
    x = reshape(features(:,j), frameSize, []);
    subplot(dispSize, dispSize, plotNo);
    plotNo = plotNo + 1;
    plot3(x(1,:), x(2,:), x(3,:), 'r');
    hold on
    plot3(x(4,:), x(5,:), x(6,:), 'g');
    plot3(x(7,:), x(8,:), x(9,:), 'b');
    plot3(x(10,:), x(11,:), x(12,:), 'c');
    plot3(x(13,:), x(14,:), x(15,:), 'm');
    %plot3(x(16,:), x(17,:), x(18,:), 'k');
    xlabel('x');
    ylabel('y');
    zlabel('z');
    hold off
end

end