%% Configure all the parameters for the complete model and run to train,
% find the performance on test data.
for foldNo = 1:9
%% Auto-Encoder Parameters
aeType = 2; % Auto Encoder Type: 1 = Sparse AE; 2 = Denoising AE
% model input/hidden/output sizes
frameSize = 30;     % not really a parameter. DO NOT change this.
framesPerWindow = 20;
hiddenSize = 120;
noOfAESamples = 10000;
% AE parameter
lambda = 1e-3;
% Sparse AE parameters
sparsityParam = 0.01;
beta = 15;
% Denoising AE parameter
noiseProbability = 0.2;
% gradient descent method options
options = struct;
options.Method = 'lbfgs';
options.maxIter = 1000;
options.display = 'on';
% save all the parameters into 'paramsAE.mat'
save('paramsAE.mat', 'aeType', 'frameSize', 'framesPerWindow', ...
    'hiddenSize', 'noOfAESamples', 'lambda', 'sparsityParam', 'beta', ...
    'noiseProbability', 'options');

%% CNN & Pooling Parameters
poolDim = 4;    % Pool Dimension
save('paramsCNN.mat', 'poolDim');

%% Softmax Parameters
softmaxLambda = 1e-4;
numClasses = 7;
softmaxOptions = struct;
softmaxOptions.maxIter = 600;
save('paramsSoftmax.mat', 'softmaxLambda', 'numClasses', 'softmaxOptions');

%% FOR K-FOLD VALIDATION ONLY! (If not for validation comment out region)
%foldNo = 3;
copyfile(strcat('data2/Fold',num2str(foldNo),'_GesturesTrain.mat'),'gestures.mat');
copyfile(strcat('data2/Fold',num2str(foldNo),'_GesturesTest.mat'),'testGestures.mat');

%% Validate data files
% Update the gesture labels from 0 to N => 1 to N+1
% Remove the gestures which are not atleast 18 frames long.
frameLimit = framesPerWindow+2; % minimum number of frames needed to consider as a gesture
trainFileName = 'gestures.mat';
testFileName = 'testGestures.mat';

% train gestures
file = matfile(trainFileName);
[gestures, labels] = checkData(file.gestures, file.labels, frameLimit);
save(trainFileName, 'gestures', 'labels')

% test gestures
file = matfile(testFileName);
[testGestures, testLabels] = checkData(file.testGestures, file.testLabels, frameLimit);
save(testFileName, 'testGestures', 'testLabels')

%% Call the scripts to Train & Predict.
aeTrain;    % AutoEncoder Train
%%
cnn;        % Convolve & Pool over Train Examples
%%
cnnTest;    % Prediction for Test samples
%%
predictionAnalysis; % Analysing the predictions (confusion matrix etc.,)

%% Save predictions; FOR K-FOLD VALIDATION ONLY! (If not for validation comment out region)
copyfile('predictions.mat', strcat('data2/Fold',num2str(foldNo), '_Predictions_v2.mat'));
copyfile('aeFeatures.mat', strcat('data2/Fold',num2str(foldNo), '_aeFeatures_v2.mat'));
copyfile('pooledFeatures.mat', strcat('data2/Fold',num2str(foldNo), '_pooledFeatures_v2.mat'));
copyfile('softmaxModel.mat', strcat('data2/Fold',num2str(foldNo), '_softmaxModel_v2.mat'));
pause(60);   % wait for a minute to cooldown cpu.
end