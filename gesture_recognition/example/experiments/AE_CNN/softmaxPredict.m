function [pred] = softmaxPredict(softmaxModel, data)

% softmaxModel - model trained using softmaxTrain
% data - the N x M input matrix, where each column data(:, i) corresponds to
%        a single test set
%
% Your code should produce the prediction matrix 
% pred, where pred(i) is argmax_c P(y(c) | x(i)).
 
% Unroll the parameters from theta
theta = softmaxModel.optTheta;  % this provides a numClasses x inputSize matrix
pred = zeros(1, size(data, 2));

%% ---------- YOUR CODE HERE --------------------------------------
%  Instructions: Compute pred using theta assuming that the labels start 
%                from 1.

% Lets find the probabilities for each example on each class
% Same as in the softmaxCost function calculate the 
% e^w(k)Tx(i)/sigma(i)[e^w(k)Tx(i)]
p = theta * data;
p = bsxfun(@minus, p, max(p,[],1));
p = exp(p);
p = bsxfun(@rdivide, p, sum(p));

% Get the largest probability from each column and note on which row (i.e
% class) that happened.
% [a, b] = max(matrix, [], 1) saves top values from each column in 'a'
% and position at which top value occured in 'b'
[topProb, predictedClass] = max(p,[],1);
pred = predictedClass';

% ---------------------------------------------------------------------

end

