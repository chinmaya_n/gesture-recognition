%% Function to validate and update data files

function [gestures, labels] = checkData(gestures, labels, frameLimit)
    % check if they are of same size
    if(size(gestures,1)==size(labels,1))
        % delete the gesture which has frames less than 'frameLimit'
        indices = [];
        for i = 1:size(gestures,1)
            if(size(gestures{i},2) < frameLimit)
                indices = [indices i];
            end
        end
        
        if (~isempty(indices))
            gestures(indices) = [];
            labels(indices) = [];
            disp 'Removed gestures which are less than threshold length';
        end
        
        % update labels from 0 to 1
        if any(0==labels)
            labels = labels + 1;
            disp 'Updated labels from 0 to N => 1 to N+1';
        end
    end
end