%% Analyse the predictions for the Test Samples - S1

predClass = ones(3,1);  % predicted class labels
testLabels = ones(3,1); % original class labels
load('predictions.mat');

%% Total predictions of all the test samples - S2
% Section 1 (S1) and Section 2 (S2) are mutually exclusive. Use only one at
% a time. Comment out the other region

% load('data2/TotalPredictions.mat')
% predClass = pc;
% testLabels = tl;

%% Print the sample numbers which went wrong
disp('Wrong Classifications: ');
for i = 1:size(predClass, 1)
    if(predClass(i,1) ~= testLabels(i,1))
        fprintf('%d - pred: %d ; true: %d \n', i, predClass(i,1), testLabels(i,1));
    end
end
disp('\n');

%% Display the Confusion matrix
groundTruth = full(sparse(testLabels, 1:size(testLabels,1), 1));
predTruth = full(sparse(predClass, 1:size(predClass,1), 1));
plotconfusion(groundTruth, predTruth);
