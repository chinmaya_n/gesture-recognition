%% Whole system Test the Gestures
for foldNo = 1:9
    %% load the variables
    testGestures = cell(1,1);% test Gestures (random initialization here)
    testLabels = [1 2];     % test labels (random initialization here)
    framesPerWindow = 0;    % 16
    frameSize = 0;          % 15
    hiddenSize = 0;         % numOfFeatures = 72
    optTheta = zeros(1,1);  % N x 1
    softmaxModel = struct;  % declaring as a struct
    poolDim = 1;            % 4

    %% Load the WS - Fold K test gesture file and model parameters
    dirLocation = 'data2/';
    copyfile(strcat(dirLocation, 'WSTestGestureRegionsFold', num2str(foldNo), '_Gestures.mat'), 'testGestures.mat');
    copyfile(strcat(dirLocation, 'Fold', num2str(foldNo), '_aeFeatures_v2.mat'), 'aeFeatures.mat');
    copyfile(strcat(dirLocation, 'Fold', num2str(foldNo), '_pooledFeatures_v2.mat'), 'pooledFeatures.mat');
    copyfile(strcat(dirLocation, 'Fold', num2str(foldNo), '_softmaxModel_v2.mat'), 'softmaxModel.mat');

    %% Load parameters
    load('aeFeatures.mat');
    load('pooledFeatures.mat');
    load('softmaxModel.mat');
    load('testGestures.mat');

    visibleSize = frameSize*framesPerWindow; % 15*16 = 240
    W1 = reshape(optTheta(1:hiddenSize*visibleSize, 1), hiddenSize, visibleSize);
    b1 = reshape(optTheta(hiddenSize*visibleSize*2+1:hiddenSize*visibleSize*2+hiddenSize, ...
                    1), hiddenSize, 1);

    %% convolve over the test gestures
    convolvedFeatures = cnnConvolve([frameSize framesPerWindow], hiddenSize, W1, b1, testGestures);

    %% pool the convolved test data features
    pooledFeatures = cnnPool(poolDim, convolvedFeatures);

    %% Predict using Softmax classifiers
    [predClass] = softmaxPredict(softmaxModel, pooledFeatures);
    predClass = predClass-1;
    
    %% save the prediction to a file
    fileID = fopen(strcat(dirLocation, 'WSTestGestureRegionsFold', num2str(foldNo), '_AE_CNN_PredLabels.txt'), 'w');
    for l = 1:size(predClass, 1)
        fprintf(fileID, '%d\n', predClass(l,1));
    end
    fclose(fileID);
    
%     accuracy = (predClass(:) == testLabels(:));
%     accuracy = sum(accuracy) / size(accuracy, 1);
%     fprintf('Accuracy: %2.3f%%\n', accuracy * 100);
%     save('predictions.mat', 'predClass', 'testLabels');
end