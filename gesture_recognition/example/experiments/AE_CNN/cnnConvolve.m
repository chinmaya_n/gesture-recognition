%% Convolve over the gestures
function convolvedFeatures = cnnConvolve(patchDim, numOfFeatures, W, b, gestures)
% Parameters:
%  patchDim: m X n - size of the patch window to slide over gestures
%  W: weights
%  b: bias
%  gestures: gesture data
% Output: convolved features

patchWidth = patchDim(1, 2);
patchHeight = patchDim(1, 1);
numOfGestures = size(gestures, 1);
convolvedFeatures = cell(numOfGestures, 1);

for gestureNum = 1:numOfGestures
    gesture = gestures{gestureNum};
    activations = zeros(numOfFeatures, size(gesture, 2)-patchWidth+1);
    for featureNum = 1:numOfFeatures
        patch = reshape(W(featureNum,:), patchHeight, patchWidth);
        patch = flipud(fliplr(squeeze(patch))); % flip to pass to the conv2
        activations(featureNum, :) = conv2(gesture, patch, 'valid') + b(featureNum, 1);
    end
    convolvedFeatures{gestureNum} = sigmoid(activations);
end

end

%% Sigmoid Function
function sigm = sigmoid(x)
    sigm = 1 ./ (1 + exp(-x));
end