%% Sample the gestures for training Auto Encoder

function data = sampleGestures(gestures, noOfFrames, noOfSamples)

% sample Gestures returns the data needed for training in size 'noOfFrames'
% Parameters:
%  gestures: all the test example gestures (noOfEgs x 1)
%  noOfFrames: from each example, number of consecutive frames to be sampled
%  noOfSamples: required number of samples
%
% Returns: data - (data per frame x noOfFrames) x noOfSamples

% variables needed
valuesPerFrame = size(gestures{1}, 1);
noOfExamples = size(gestures, 1);
data = zeros(valuesPerFrame*noOfFrames, noOfSamples);

% get the samples
for n = 1:noOfSamples
    windowRange = 1;    % sample starting value
    while windowRange < 2
        sampleNo = randi([1 noOfExamples]);
        currentEg = gestures{sampleNo};
        windowRange = size(currentEg, 2)-noOfFrames+1;
    end

    currentWindow = randi([1 windowRange]);
    data(:,n) = reshape(currentEg(:,currentWindow:currentWindow+noOfFrames-1),[], 1);
end

end