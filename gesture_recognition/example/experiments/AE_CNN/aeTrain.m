%% Train the Sparse Auto Encoder

%% Parameters for the model - Reloaded as saved in "start.m"
% dummy initializations.(loaded from aeParams.mat ~ start.m)
framesPerWindow = 1;
frameSize = 1;
hiddenSize = 1;
noOfAESamples = 5;
aeType = 2; % Auto Encoder Type: 1 = Sparse AE; 2 = Denoising AE
lambda = 0;
sparsityParam = 0;
beta = 1;
noiseProbability = 0;
options = struct; % gradient descent method options
% Load the above variables with corresponding values
load('paramsAE.mat');
visibleSize = frameSize*framesPerWindow; % 18*16 = 288

%% Load the data & Get the training samples
gestures = cell(10, 1); % random initialization
labels = zeros(10, 1);  % random initialization
load('gestures.mat');   % loads gestures, labels variables
aeTrainData = sampleGestures(gestures, framesPerWindow, noOfAESamples);

%% Learn Features
theta = initializeParameters(hiddenSize, visibleSize);
addpath ./minFunc/; % to use minFunc options

if (aeType == 1)
    [optTheta, cost] = minFunc( @(p) sparseAutoencoderCost(p, visibleSize, ...
        hiddenSize, lambda, sparsityParam, beta, ...
        aeTrainData), theta, options);
elseif (aeType == 2)
    % corrupt the data (i.e. add noise to input data)
    noiseData = zeros(size(aeTrainData));
    noOfInputEgs = size(aeTrainData, 2);   % 5000 or so
    inputSize = size(aeTrainData, 1);      % 18 * 16
    for i = 1:noOfInputEgs
        noiseData(:,i) = arrayfun(@(z) sum(z >= cumsum([0, noiseProbability])), ...
            rand(inputSize,1)) - 1;        % see link http://stackoverflow.com/questions/13914066/generate-random-number-with-given-probability-matlab
    end
    
    [optTheta, cost] = minFunc(@(p) denoisingAutoencoderCost(p, visibleSize, ...
        hiddenSize, lambda, noiseData, aeTrainData), theta, options);
end

% Saving learned features
save('aeFeatures.mat', 'optTheta', 'framesPerWindow', 'frameSize', 'hiddenSize');
fprintf('Features Saved!\n');

%% Display Features Learned
W1 = reshape(optTheta(1:hiddenSize*visibleSize,1), hiddenSize, visibleSize);
features = displayFeatures(W1);