%% Finds the cost, gradient of SAE for given parameters

function [cost,grad] = denoisingAutoencoderCost(theta, visibleSize, hiddenSize, ...
                                             lambda, noiseData, data)

% Parameters:
% visibleSize: the number of input units
% hiddenSize: the number of hidden units
% lambda: weight decay parameter
% noiseProbability: probability of noise to be induced into each example (like 0.3, 0.5)
% data: Our matrix containing the training data.  So, data(:,i) is the i-th training example. 

% Output: cost, gradient for the given parameters

% The input theta is a vector (because minFunc expects the parameters to be a vector). 
% We first convert theta to the (W1, W2, b1, b2) matrix/vector format
% 

W1 = reshape(theta(1:hiddenSize*visibleSize), hiddenSize, visibleSize);
W2 = W1';
b1 = theta(2*hiddenSize*visibleSize+1:2*hiddenSize*visibleSize+hiddenSize);
b2 = theta(2*hiddenSize*visibleSize+hiddenSize+1:end);
noiseData = data .* noiseData;

% Cost and gradient variables (your code needs to compute these values). 
% Here, we initialize them to zeros. 
cost = 0;
W1grad = zeros(size(W1)); 
W2grad = zeros(size(W2));
b1grad = zeros(size(b1)); 
b2grad = zeros(size(b2));

%  Instructions: Compute the cost/optimization objective J_sparse(W,b) for the Sparse Autoencoder,
%                and the corresponding gradients W1grad, W2grad, b1grad, b2grad.
%
% W1grad, W2grad, b1grad and b2grad should be computed using backpropagation.
% Note that W1grad has the same dimensions as W1, b1grad has the same dimensions
% as b1, etc.  Your code should set W1grad to be the partial derivative of J_sparse(W,b) with
% respect to W1.  I.e., W1grad(i,j) should be the partial derivative of J_sparse(W,b) 
% with respect to the input parameter W1(i,j).  Thus, W1grad should be equal to the term 
% [(1/m) \Delta W^{(1)} + \lambda W^{(1)}] in the last block of pseudo-code in Section 2.2 
% of the lecture notes (and similarly for W2grad, b1grad, b2grad).
% 
% Stated differently, if we were using batch gradient descent to optimize the parameters,
% the gradient descent update to W1 would be W1 := W1 - alpha * W1grad, and similarly for W2, b1, b2. 
% 

% For computing cost we have three elements: mean-square error + weight decay term

% mean - square error
noOfInputEgs = size(data, 2);   % 5000 or so
a2 = sigmoid(W1*noiseData + repmat(b1, 1, noOfInputEgs));  % a2 = func(W1 * data)
a3 = sigmoid(W2*a2 + repmat(b2, 1, noOfInputEgs));  % a3 = func(W2 * a2)
distV = a3 - data;  % distance b/w predicted X to real (we want this to be very small)
meanSquareError = sum(sum(distV .* distV))/(2*noOfInputEgs);

% weight decay term
weightDecay = (lambda/2) * (sum(sum(W1.*W1)) + sum(sum(W2.*W2)));

% Cost computation
cost = meanSquareError + weightDecay;

% Compute Gradients: We need to find gradient wrt W1, W2, b1, b2

% W1grad
delta3 = (a3 - data) .* (a3 .* (1-a3)); % delta3 = (h-y)*f'(z); Here, f'(x) = f(x)(1-f(x)); 288 x 5000
delta2 = (W2'*delta3) .* (a2 .* (1-a2)); % 72 x 5000 ;
W1grad = lambda*W1 + (1/noOfInputEgs)*(delta2 * noiseData'); % 72 x 288

% W2grad
W2grad = lambda*W2 + (1/noOfInputEgs)*(delta3 * a2'); % 288 x 72

% b1grad
b1grad = sum(delta2, 2)/noOfInputEgs;

% b2grad
b2grad = sum(delta3, 2)/noOfInputEgs;

%-------------------------------------------------------------------
% After computing the cost and gradient, we will convert the gradients back
% to a vector format (suitable for minFunc).  Specifically, we will unroll
% your gradient matrices into a vector.

grad = [W1grad(:) ; W2grad(:) ; b1grad(:) ; b2grad(:)];

end

%-------------------------------------------------------------------
% Here's an implementation of the sigmoid function, which you may find useful
% in your computation of the costs and the gradients.  This inputs a (row or
% column) vector (say (z1, z2, z3)) and returns (f(z1), f(z2), f(z3)). 

function sigm = sigmoid(x)
  
    sigm = 1 ./ (1 + exp(-x));
end
