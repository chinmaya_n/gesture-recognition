%% Convolving & Pooling of the gestures

%% Load the variables (optTheta, gestures etc)
% Dummy Variables - Loaded from .mat files
gestures = cell(1,1);
framesPerWindow = 0;    % 16
frameSize = 0;          % 18
hiddenSize = 0;         % numOfFeatures = 72
optTheta = zeros(1,1);  % N x 1
poolDim = 1;    % Pool Dimension
% load the mat files
load('gestures.mat');
load('aeFeatures.mat');
load('paramsCNN.mat');

visibleSize = frameSize*framesPerWindow; % 18*16 = 288
W1 = reshape(optTheta(1:hiddenSize*visibleSize, 1), hiddenSize, visibleSize);
b1 = reshape(optTheta(hiddenSize*visibleSize*2+1:hiddenSize*visibleSize*2+...
            hiddenSize, 1), hiddenSize, 1);

%% Convolve
% cnnConvolve(patchDim, numOfFeatures, W, b, gestures)
convolvedFeatures = cnnConvolve([frameSize framesPerWindow], hiddenSize, ...
                            W1, b1, gestures);

%% Pool the convolved Features
pooledFeatures = cnnPool(poolDim, convolvedFeatures);
save('pooledFeatures.mat', 'pooledFeatures', 'labels', 'poolDim');

%% Softmax Train on pooled features
% sample initialization for softmax parameters. Loaded from paramsSoftmax.mat
softmaxLambda = 1;
numClasses = 1;
softmaxOptions = struct;
% load the variables
load('paramsSoftmax.mat');

% train softmax classifier
try
softmaxModel = softmaxTrain(size(pooledFeatures, 1), numClasses, ...
            softmaxLambda, pooledFeatures, labels, softmaxOptions);
catch
    fprintf('')
end
save('softmaxModel.mat', 'softmaxModel');
