"""Run the "start.m" script file in matlab to run the whole project."""

It has all the parameters required for the model already in one place.
Just change them to you liking and hit run. It goes step by step from 
learning procedure: Auto Encoder -> Convolving -> Pooling -> Softmax classifier
to testing procedure: CNN -> Pool -> Softmax and compares with true labels and
displays a confusion matrix for you.

"You just need to configure the parameters in start.m and hit run"