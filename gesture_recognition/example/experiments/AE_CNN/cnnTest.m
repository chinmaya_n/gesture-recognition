%% Test the Gestures
% load the variables
testGestures = cell(1,1);% test Gestures (random initialization here)
testLabels = [1 2];     % test labels (random initialization here)
framesPerWindow = 0;    % 16
frameSize = 0;          % 15
hiddenSize = 0;         % numOfFeatures = 72
optTheta = zeros(1,1);  % N x 1
softmaxModel = struct;  % declaring as a struct
poolDim = 1;            % 4

load('aeFeatures.mat');
load('pooledFeatures.mat');
load('softmaxModel.mat');
load('testGestures.mat');

visibleSize = frameSize*framesPerWindow; % 15*16 = 240
W1 = reshape(optTheta(1:hiddenSize*visibleSize, 1), hiddenSize, visibleSize);
b1 = reshape(optTheta(hiddenSize*visibleSize*2+1:hiddenSize*visibleSize*2+hiddenSize, ...
                1), hiddenSize, 1);

%% convolve over the test gestures
convolvedFeatures = cnnConvolve([frameSize framesPerWindow], hiddenSize, W1, b1, testGestures);

%% pool the convolved test data features
pooledFeatures = cnnPool(poolDim, convolvedFeatures);

%% Predict using Softmax classifiers
[predClass] = softmaxPredict(softmaxModel, pooledFeatures);
accuracy = (predClass(:) == testLabels(:));
accuracy = sum(accuracy) / size(accuracy, 1);
fprintf('Accuracy: %2.3f%%\n', accuracy * 100);
save('predictions.mat', 'predClass', 'testLabels');