%% Pool the convolved features

function pooledFeatures = cnnPool(poolDim, convolvedFeatures)
% Parameters:
%  poolDim: dimension of the pooling region - eg: 4 (i.e make into 4 parts)
%  convolvedFeatures: cnnConvolve output
% Output: pooled features for the given convolved featues

% variables
numOfFeatures = size(convolvedFeatures{1}, 1);
numOfGestures = size(convolvedFeatures, 1);
pooledFeatures = zeros(numOfFeatures*poolDim, numOfGestures);

% pool the features for each example and for each feature
try
for gestureNum = 1:numOfGestures
    convGesture = convolvedFeatures{gestureNum};
    step = ceil(size(convGesture, 2)/poolDim);
    i = 0;  % pooledFeature(i, j) - i'th pooled value, for gesture j
    for featureNum = 1:numOfFeatures
        pooledFeatures(i+1, gestureNum) = max(convGesture(featureNum, step*0+1:step*0+step));
        pooledFeatures(i+2, gestureNum) = max(convGesture(featureNum, step*1+1:step*1+step));
        pooledFeatures(i+3, gestureNum) = max(convGesture(featureNum, step*2+1:step*2+step));
        pooledFeatures(i+4, gestureNum) = max(convGesture(featureNum, step*3+1:end));
        i = i + 4;
    end
end
catch
    fprintf('')
end
end
