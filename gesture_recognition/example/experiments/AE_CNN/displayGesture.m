%% Display/Draw the Gesture in 3D
function result = displayGesture(gesture)
% display the given gesture in 3D

figure;
plot3(gesture(1,:), gesture(2,:), gesture(3,:), 'r');
hold on
plot3(gesture(4,:), gesture(5,:), gesture(6,:), 'g');
plot3(gesture(7,:), gesture(8,:), gesture(9,:), 'b');
plot3(gesture(10,:), gesture(11,:), gesture(12,:), 'c');
plot3(gesture(13,:), gesture(14,:), gesture(15,:), 'm');
plot3(gesture(16,:), gesture(17,:), gesture(18,:), 'k');
xlabel('gesture');
ylabel('y');
zlabel('z');
hold off

end