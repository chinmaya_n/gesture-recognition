function [cost, grad] = softmaxCost(theta, numClasses, inputSize, lambda, data, labels)

% numClasses - the number of classes 
% inputSize - the size N of the input vector
% lambda - weight decay parameter
% data - the N x M input matrix, where each column data(:, i) corresponds to
%        a single test set
% labels - an M x 1 matrix containing the labels corresponding for the input data
%

% Unroll the parameters from theta
theta = reshape(theta, numClasses, inputSize);

numCases = size(data, 2);

groundTruth = full(sparse(labels, 1:numCases, 1));
cost = 0;

thetagrad = zeros(numClasses, inputSize);

%% ---------- YOUR CODE HERE --------------------------------------
%  Instructions: Compute the cost and gradient for softmax regression.
%                You need to compute thetagrad and cost.
%                The groundTruth matrix might come in handy.

% Lets find wTx for each example & for each class
f = theta * data;   % K x M matrix (value for each class over each example)

% To control the overflow in the given function, subtract each element 
% with the largest element.
% * max function below computes the largest element in column and prepares 
% a row matrix
f = bsxfun(@minus, f, max(f,[],1));

% Now find the e^w(k)Tx(i) for each element
h = exp(f);         % e ^ each element in matrix f

% Now we have to do the e^w(k)Tx(i)/sigma(i)[e^w(k)Tx(i)]
% i.e divide by the sum of all e^ for that example from each class
h = bsxfun(@rdivide, h, sum(h));

% Now lets compute the cost
% kroneker-delta func * log(h) for each class for each example + weight decay term
cost = (-1/numCases)*sum(sum(groundTruth.*log(h))) + (lambda/2)*sum(sum(theta.^2));

% Now lets calculate gradient
% sum of (delta_func - probability) for each example on a single class
% each row in thetagrad is a w(k)
thetagrad = (-1/numCases)*((groundTruth-h)*data') + (lambda*theta);

% ------------------------------------------------------------------
% Unroll the gradient matrices into a vector for minFunc
grad = [thetagrad(:)];
end

