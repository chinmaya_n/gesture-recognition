%% Convert the data in the text file to .mat file

% training or test data
trainData = 0;  % 1 = Train data / 0 = Test data
testData = 0;
validData = 1;
frameLength = 30;   % no. of values in each frame of a gesture

% files
dirLocation = './data2/WSTestGestureRegionsFold';
fileExt = '.txt';
% foldNum = 0;

%%
for foldNum = 1:9
    % Open the file
    data_file = fopen(strcat(dirLocation,num2str(foldNum),fileExt));
    line = fgetl(data_file);

    % create the variables where you want to save the gestures data & labels
    noOfGestures = str2num(line);
    gestures = cell(noOfGestures,1);
    labels = zeros(noOfGestures,1);

    % normalize the data as well : [-1 to 1] range
    % X-Axis max value is 500, Y-Axis : 600, Z-Axis : 400
    maxVals = [500; 600; 400];

    % read each line and save them into variables
    line = fgetl(data_file);
    count = 1;
    while ischar(line)
        example = str2num(line);
        labels(count) = example(1,1);
        gestures{count} = reshape(example(1,2:end), frameLength, []);
        % normalize
        normVec = repmat(maxVals, frameLength/3, 1);
        gestures{count} = bsxfun(@rdivide, gestures{count}, normVec);
        % prepare for next iteration
        line = fgetl(data_file);    % get next line from the file
        count = count + 1;  % increment the no of examples count
    end

    % remove extra cells created
    extraCells = noOfGestures - (count -1);
    if(extraCells>0)
        gestures = gestures(~cellfun(@isempty, gestures));
        labels = labels(1:(noOfGestures-extraCells),1);
    end
    
    % save gestures and label variables in .mat file
    testGestures = gestures;
    testLabels = labels;
    save(strcat(dirLocation,num2str(foldNum),'_Gestures.mat'), 'testGestures', 'testLabels');
end

%% NOTE
% if the no of gestures (1st line - noOfGestures) is less than the real number of
% gestures (no of lines) in the file then error occurs