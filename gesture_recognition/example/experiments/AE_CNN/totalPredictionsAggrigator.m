%% Aggregates the results from all the K-fold predictions
pc = [];
tl = [];
for foldNo = 1:9
    f = matfile(strcat('./data2/Fold', num2str(foldNo), '_Predictions.mat'));
    pc = vertcat(pc, f.predClass);
    tl = vertcat(tl, f.testLabels);
end
save('./data2/TotalPredictions.mat', 'pc', 'tl');