from __future__ import print_function
from sklearn.model_selection import KFold, StratifiedKFold
import numpy

def generateKFoldFilesGD(framesFile, framesLabelsFile, k, toSaveLoc):
    """Generates K fold files for GD data."""
    # open the files
    fpFramesFile = open(framesFile) #with open(framesFile) as fp:
    fpFramesLabelsFile = open(framesLabelsFile)
    # get no of recordings
    noOfRecordings = int(fpFramesFile.readline())
    foldSize = int(round(noOfRecordings/(1.0*k)))
    trainTestSize = noOfRecordings-foldSize
    validationIndex = numpy.array(range(foldSize))+trainTestSize
    kf = KFold(n_splits=k-1)
    foldNo = 0
    # close file pointers
    fpFramesFile.close()
    fpFramesLabelsFile.close()    
    # for each Fold
    for trainIndex, testIndex in kf.split(X=numpy.array(range(trainTestSize))):
        # open data files
        foldNo += 1
        fpFramesFile = open(framesFile)
        fpFramesLabelsFile = open(framesLabelsFile)
        fpFramesFile.readline()
        fpFramesLabelsFile.readline()
        # open fold files
        fpFoldTrainFramesFile = open(toSaveLoc+"Fold"+str(foldNo)+"FramesTrain.txt", mode='w')
        fpFoldTestFramesFile = open(toSaveLoc+"Fold"+str(foldNo)+"FramesTest.txt", mode='w')
        fpFoldTrainFramesLabelsFile = open(toSaveLoc+"Fold"+str(foldNo)+"FramesLabelsTrain.txt", mode='w')
        fpFoldTestFramesLabelsFile = open(toSaveLoc+"Fold"+str(foldNo)+"FramesLabelsTest.txt", mode='w')
        if foldNo == 1:
            fpFoldValidFramesLabelsFile = open(toSaveLoc+"Fold"+"FramesLabelsValid.txt", mode='w')
            fpFoldValidFramesFile = open(toSaveLoc+"Fold"+"FramesValid.txt", mode='w')
        
        # write the first line for each file
        fpFoldTrainFramesFile.write(str(len(trainIndex))+"\n")
        fpFoldTestFramesFile.write(str(len(testIndex))+"\n")
        fpFoldTrainFramesLabelsFile.write(str(len(trainIndex))+"\n")
        fpFoldTestFramesLabelsFile.write(str(len(testIndex))+"\n")
        if foldNo == 1:
            fpFoldValidFramesFile.write(str(len(validationIndex))+"\n")
            fpFoldValidFramesLabelsFile.write(str(len(validationIndex))+"\n")
        
        # write each example to files
        for i, line in enumerate(fpFramesFile):
            if i in trainIndex:
                fpFoldTrainFramesFile.write(line)
            elif i in testIndex:
                fpFoldTestFramesFile.write(line)
            elif i in validationIndex:
                if foldNo == 1:
                    fpFoldValidFramesFile.write(line)
        for i, line in enumerate(fpFramesLabelsFile):
            if i in trainIndex:
                fpFoldTrainFramesLabelsFile.write(line)
            elif i in testIndex:
                fpFoldTestFramesLabelsFile.write(line)
            elif i in validationIndex:
                if foldNo == 1:
                    fpFoldValidFramesLabelsFile.write(line)
        
        # close all file pointers
        fpFoldTrainFramesFile.close()
        fpFoldTestFramesFile.close()
        fpFoldTrainFramesLabelsFile.close()
        fpFoldTestFramesLabelsFile.close()
        fpFramesFile.close()
        fpFramesLabelsFile.close()
        if foldNo == 1:
            fpFoldValidFramesFile.close()
            fpFoldValidFramesLabelsFile.close()

def generateKFoldFilesGC(gesturesFile, k, saveLoc):
    """Generate K Fold files for Gesture Classification."""
    # open the file to find number of gestures
    fpGesturesFile = open(gesturesFile)
    noOfGestures = int(fpGesturesFile.readline())
    foldSize = int(round(noOfGestures/(1.0*k)))
    trainTestSize = noOfGestures-foldSize
    validationIndex = numpy.array(range(foldSize))+trainTestSize
    # get the labels from file for stratified split -- Not an ideal way as you will 
    # have to read the completefile twice (once here & once below).
    trueLabels = []
    for line in fpGesturesFile.readlines():
        words = line.split(" ")
        trueLabels.append(int(words[0]))
    fpGesturesFile.close()  # close file
    # stratified split
    skf = StratifiedKFold(n_splits=k-1)
    # kf = KFold(n_splits=k-1)
    # kf.split(X=numpy.array(range(trainTestSize))):
    foldNo = 0
    # for each fold
    for trainIndex, testIndex in skf.split(X=numpy.array(range(trainTestSize)), y=trueLabels[0:trainTestSize]):
        # open data files
        foldNo += 1
        fpGesturesFile = open(gesturesFile)
        fpGesturesFile.readline()
        # open fold files
        fpFoldTrainFile = open(saveLoc+"Fold"+str(foldNo)+"GesturesTrain.txt", mode='w')
        fpFoldTestFile = open(saveLoc+"Fold"+str(foldNo)+"GesturesTest.txt", mode='w')
        if foldNo==1:
            fpFoldValidFile = open(saveLoc+"Fold"+"GesturesValid.txt", mode='w')
        # write the count to each file
        fpFoldTrainFile.write(str(len(trainIndex))+"\n")
        fpFoldTestFile.write(str(len(testIndex))+"\n")
        if foldNo==1:
            fpFoldValidFile.write(str(len(validationIndex))+"\n")
        
        # write each example to files
        for i, line in enumerate(fpGesturesFile):
            if i in trainIndex:
                fpFoldTrainFile.write(line)
            elif i in testIndex:
                fpFoldTestFile.write(line)
            elif i in validationIndex:
                if foldNo == 1:
                    fpFoldValidFile.write(line)
        
        # close all the file pointers
        fpGesturesFile.close()
        fpFoldTrainFile.close()
        fpFoldTestFile.close()
        if foldNo==1:
            fpFoldValidFile.close()
    

if __name__=="__main__":
    generateKFoldFilesGD(framesFile="./data/GD_FramesData.txt",
                         framesLabelsFile="./data/GD_FramesLabels_no2.txt",
                         k=10, toSaveLoc="./data/")
    
    generateKFoldFilesGC(gesturesFile="../data/GC_GesturesData.txt", k=10, 
                         saveLoc="./data/")
