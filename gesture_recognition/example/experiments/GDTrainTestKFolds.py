from __future__ import print_function
from sklearn.metrics import precision_recall_fscore_support
import numpy
from gesture_recognition.mlcore.GestureDetectionTraining import GestureDetectionTraining
from gesture_recognition.mlcore.GestureDetectionPredict import GestureDetectionPredict

from config import Config as config

def postProcessLabels(labels):
    """ Post process the given labels with params from config. """
    # post process variables
    gestureInProgress = False
    gestureStartPosition = -1
    gestureEndPosition = -1
    gestureEndFramesCount = 0
    gestureFramesCount = 0
    # process the labels
    for i in range(len(labels)):
        frameLabel = labels[i]
        if frameLabel==0:
            if gestureInProgress==True:
                gestureEndFramesCount += 1
                gestureFramesCount += 1
                if gestureEndFramesCount >= config.frame_labels_patience: # end of gesture
                    gestureEndPosition = i
                    # found a gesture. post process the labels
                    if gestureFramesCount > config.gesture_frames_count_threshold:
                        beginPosition = gestureStartPosition-config.boundary_inclusion
                        endPosition = gestureEndPosition+1
                        for pos in range(endPosition-beginPosition):
                            labels[pos+beginPosition] = 1
                        #labels.__setslice__(gestureStartPosition-config.boundary_inclusion,
                                            #gestureEndPosition+1,
                                            #numpy.ones((gestureEndPosition-gestureStartPosition+config.boundary_inclusion+1,), dtype=int))
                    # reset params
                    gestureInProgress = False
                    gestureEndFramesCount = 0
                    gestureFramesCount = 0
            #else:
                ## maintain the gesture begin number of frames (excluding the first frame - FIFO)
                #self.startFrames = numpy.concatenate((self.startFrames, frame), axis=1)[:,1:]
        elif frameLabel==1:
            if gestureInProgress==False:
                gestureStartPosition = i
                gestureFramesCount = 0
            gestureInProgress = True
            gestureEndFramesCount = 0
            gestureFramesCount += 1
    # remove the gesture regions which are smaller than threshold length (config.gesture_frames_count_threshold)
    gestureLength = 0
    gestureInProgress = False
    for i in range(len(labels)):
        frameLabel = labels[i]
        if frameLabel == 1 and gestureInProgress == False:
            gestureInProgress = True
            gestureStartPosition = i
            gestureLength += 1
        elif frameLabel == 1 and gestureInProgress == True:
            gestureLength += 1
        elif frameLabel == 0 and gestureInProgress == True:
            gestureInProgress = False
            gestureEndPosition = i+1
            if gestureLength <= config.gesture_frames_count_threshold:
                print("Found a small gesture region, deleting it (below threshold).")
                for pos in range(gestureEndPosition-gestureStartPosition):
                    labels[pos+gestureStartPosition] = 0
            # reset
            gestureLength = 0
            gestureStartPosition = 0
            gestureEndPosition = 0

def getPredLabels():
    """ Get the predicted labels for all the folds. """
    # for each fold, train
    for foldNo in range(10)[1:]:
        # Labels
        trueLabels = []
        predLabels = []
        # data files
        config.gd_train_frames_file = "./data3/Fold"+str(foldNo)+"FramesTrain.txt"
        config.gd_train_labels_file = "./data3/Fold"+str(foldNo)+"FramesLabelsTrain.txt"
        config.gd_test_frames_file = "./data3/Fold"+str(foldNo)+"FramesTest.txt"
        config.gd_test_labels_file = "./data3/Fold"+str(foldNo)+"FramesLabelsTest.txt"
        config.gd_valid_frames_file = "./data3/FoldFramesValid.txt"
        config.gd_valid_labels_file = "./data3/FoldFramesLabelsValid.txt"
        # params
        config.save_params_to = "./data3/GD_Params_Fold"+str(foldNo)+".npz"
        config.load_params_from = config.save_params_to

        # train
        gdt = GestureDetectionTraining(config)

        # predict
        gdp = GestureDetectionPredict(config, config.load_params_from)

        # test it and record the labels
        for eg_id in range(len(gdt.test_data[1])):
            gdp.reset_memory_cells()
            trueLabels.extend(gdt.test_data[1][eg_id])
            # iterate for each frame
            for f_id in range(gdt.test_data[0][eg_id].shape[1]):
                frame = numpy.matrix(gdt.test_data[0][eg_id][:,f_id]).transpose()
                predLabels.extend([gdp.predict_label(frame).tolist()])
            # check if true & pred labels are having same length to make sure
            if len(trueLabels) != len(predLabels):
                print("Fatal Error! True & Pred Labels arrays sizes differ. In eg_id: ", eg_id)
                exit()

        # save the labels
        saveLabelsFile = "./data3/LabelsFold"+str(foldNo)+".npz"
        numpy.savez(saveLabelsFile, trueLabels=trueLabels, predLabels=predLabels)

        # post process the pred labels and save
        saveLabelsFile = "./data3/PostProcLabelsFold"+str(foldNo)+".npz"
        postProcessLabels(predLabels)
        numpy.savez(saveLabelsFile, trueLabels=trueLabels, predLabels=predLabels)

def calculatePRF():
    """ Calculate Precision-Recall-FScore. """
    # -- Before Post processing
    # get all the labels across all folds
    trueLabels = []
    predLabels = []
    for foldNo in range(10)[1:]:
        labels = numpy.load("./data3/LabelsFold"+str(foldNo)+".npz")
        trueLabels.extend(labels['trueLabels'])
        predLabels.extend(labels['predLabels'])
    # send it to precision & recall calculation
    result = precision_recall_fscore_support(y_true=trueLabels, y_pred=predLabels)
    print("Before Post Processing Predicted Labels: ")
    print(result)

    # -- After Post processing
    # get all the labels across all folds
    trueLabels = []
    predLabels = []
    for foldNo in range(10)[1:]:
        labels = numpy.load("./data3/PostProcLabelsFold"+str(foldNo)+".npz")
        trueLabels.extend(labels['trueLabels'])
        predLabels.extend(labels['predLabels'])
    # send it to precision & recall calculation
    result = precision_recall_fscore_support(y_true=trueLabels, y_pred=predLabels)
    print("After Post Processing Predicted Labels: ")
    print(result)

if __name__=="__main__":
    #getPredLabels()
    calculatePRF()

    # # #
    #for foldNo in range(10)[1:]:
        #labelsFile = "./data3/LabelsFold"+str(foldNo)+".npz"
        #trueLabels = numpy.load(labelsFile)['trueLabels']
        #predLabels = numpy.load(labelsFile)['predLabels']
        ## post process the pred labels and save
        #postProcessLabels(predLabels)
        ##print(len(trueLabels), len(predLabels))
        #saveLabelsFile = "./data3/PostProcLabelsFold"+str(foldNo)+".npz"
        #numpy.savez(saveLabelsFile, trueLabels=trueLabels, predLabels=predLabels)
