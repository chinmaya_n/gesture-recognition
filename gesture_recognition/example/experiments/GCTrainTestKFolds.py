from __future__ import print_function
from sklearn.metrics import precision_recall_fscore_support, confusion_matrix
import numpy
from gesture_recognition.mlcore.GestureClassificationTraining import GestureClassificationTraining
from gesture_recognition.mlcore.GestureClassificationPredict import GestureClassificationPredict

from config import Config as config

def generatePredLabels():
    """ Get the predicted labels for all the folds. """
    # train the GC model for each fold
    for foldNo in range(10)[1:]:
        # labels
        trueLabels = []
        predLabels = []
        # data files
        config.gc_train_gestures_file = "./data2/Fold"+str(foldNo)+"GesturesTrain.txt"
        config.gc_test_gestures_file = "./data2/Fold"+str(foldNo)+"GesturesTest.txt"
        config.gc_valid_gestures_file = "./data2/FoldGesturesValid.txt"
        # params
        config.gc_save_params_to = "./data2/GC_Params_Fold"+str(foldNo)+".npz"
        config.gc_load_params_from = config.gc_save_params_to

        # train
        gct = GestureClassificationTraining(config)

        # predict object
        gcp = GestureClassificationPredict(config, config.gc_load_params_from)

        # test it and record the labels
        for eg_id in range(len(gct.test_data[1])):
            trueLabels.extend([gct.test_data[1][eg_id]])
            predLabels.extend(gcp.predict_label(gct.test_data[0][eg_id]).tolist())

        # save the labels
        saveLabelsFile = "./data2/GestureLabelsFold"+str(foldNo)+".npz"
        numpy.savez(saveLabelsFile, trueLabels=trueLabels, predLabels=predLabels)

def calculatePRF():
    """ Calculate Precision-Recall-FScore. """
    # get all the labels across all folds
    trueLabels = []
    predLabels = []
    for foldNo in range(10)[1:]:
        labels = numpy.load("./data2/GestureLabelsFold"+str(foldNo)+".npz")
        trueLabels.extend(labels['trueLabels'])
        predLabels.extend(labels['predLabels'])
    # set true labels to start range from 0
    trueLabels = numpy.array(trueLabels)-1
    # send it to precision & recall calculation
    result = precision_recall_fscore_support(y_true=trueLabels, y_pred=predLabels)
    print("Gesture Labels Precision-Recall-FScore: ")
    print(result)

def createConfusionMatrix():
    """ Create a confusion matrix from the real & predicted labels. """
    # get all the labels across all folds
    trueLabels = []
    predLabels = []
    for foldNo in range(10)[1:]:
        labels = numpy.load("./data2/GestureLabelsFold"+str(foldNo)+".npz")
        trueLabels.extend(labels['trueLabels'])
        predLabels.extend(labels['predLabels'])
    # set true labels to start range from 0
    trueLabels = numpy.array(trueLabels)-1
    # confusion matrix
    result = confusion_matrix(y_true=trueLabels, y_pred=predLabels)
    print("Confusion Matrix:")
    print(result)

if __name__=="__main__":
    #generatePredLabels()
    calculatePRF()
    createConfusionMatrix()
