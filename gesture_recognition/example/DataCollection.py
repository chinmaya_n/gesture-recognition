from __future__ import print_function
from gesture_recognition.data_collection.RecordFrames import RecordFrames
from gesture_recognition.data_collection.Annotator import Annotator
from gesture_recognition.data_collection.DBtoFile import DBtoFile

# variables
mongodb_connection_string = "mongodb://localhost:27017/"
mongodb_name = "frames_data"
mongodb_collection = "trail_training" # "mock_training"
recordings_video_format = "webm"    # mp4, webm etc
video_record_pattern="/home/inblueswithu/Videos/Webcam/2016-*.webm" # recordings happen here with given pattern
video_move_loc="/home/inblueswithu/Videos/LeapRecordings/" # recordings are moved here after renaming to match db documents

location_root = "/home/inblueswithu/Documents/gesture_recognition/gesture_recognition/"
frames_data_file = location_root+"example/data/GD_FramesData_run2.txt"
frames_labels_file = location_root+"example/data/GD_FramesLabels_run2.txt"
frames_data_file_inclTrans = location_root+"example/data/GD_FramesData_inclTrans_run2.txt"
frames_labels_file_inclTrans = location_root+"example/data/GD_FramesLabels_inclTrans_run2.txt"
gestures_file = location_root+"example/data/GC_GesturesData_run2.txt"

step = 0   # 1 - Record Frames ; 2 - Annotation ; 3 - DB to data file generation 

if step==1:
    # Record Frames
    RecordFrames(mdb_connection_string=mongodb_connection_string, 
                 mdb_name=mongodb_name,
                 mdb_collection_name=mongodb_collection,
                 video_record_pattern=video_record_pattern,# recordings happen here
                 video_move_loc=video_move_loc, # recordings are moved here after renaming to match db documents
                 video_format=recordings_video_format)
elif step==2:
    # Annotate recordings
    ann = Annotator(video_files_location=video_move_loc, 
                    video_format=recordings_video_format, 
                    db_connection_string=mongodb_connection_string, 
                    db_name=mongodb_name, 
                    db_collection=mongodb_collection)
elif step==3:
    # DB to data files
    d2f = DBtoFile(mongodb_connection_string, mongodb_name, mongodb_collection)
    d2f.frames_data(frames_data_file=frames_data_file, frames_labels_file=frames_labels_file)
    d2f.frames_data(frames_data_file=frames_data_file_inclTrans, 
                    frames_labels_file=frames_labels_file_inclTrans, 
                    strict_gestures=False)
    d2f.gestures_data(gestures_file=gestures_file)
else:
    print("Step value is wrong. Please check.")
