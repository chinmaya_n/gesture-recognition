from __future__ import print_function
import Leap, numpy, sys

from FramesHandler import FramesHandler

class LeapFramesListener(Leap.Listener):
    """
    Realtime Leap frames listener to detect gestures.
    """
    paramsFileGD="./data/GD_params_run2.npz"
    paramsFileGC="./data/GC_params_run2.npz"
    
    def on_init(self, controller):
        print("LeapMotion Initialized.")
        self.fh = FramesHandler(paramsFileGD=self.paramsFileGD, paramsFileGC=self.paramsFileGC)
        print("Realtime: GDP initialized.")
        self.start_rec = True
        self.count_invalid_frames = 0
        self.last_time_stamp = 0

    def on_connect(self, controller):
        # LeapMotion Device connected
        print("LeapMotion Device Connected")

    def on_disconnect(self, controller):
        # LeapMotion Disconnected
        print("LeapMotion Device Disconnected")

    def on_exit(self, controller):
        # Program Exiting
        print("Exited the LeapMotion Controller!")

    def on_frame(self, controller):
        """
        Work to done on current frame.
        """
        frame = controller.frame()
        
        # test for recent frames. if not reset memory
        #if frame.timestamp-self.last_time_stamp > 1000 and self.last_time_stamp!=0:
            #self.fh.reset_memory()
            #sys.stdout.write("\n** LSTM cells memory reset **\n")
            #sys.stdout.flush()
            #return
        
        if self.start_rec and frame.is_valid:            
            # Fingers
            fingers = frame.fingers
            
            # test for fingers present
            if len(fingers) != 5:
                return
            # only one hand is expected for the gestures for now
            if len(frame.hands)==1:
                pass
            else:
                print("More than one hand is observed. Only single hand gestures are premitted for now!")
                return
            
            # frame data
            finger_pos = []
            finger_vel = []
            for ftype in range(5):
                for finger in fingers:
                    if(ftype==finger.type):
                        finger_pos.extend([finger.tip_position.x, finger.tip_position.y, finger.tip_position.z])
                        finger_vel.extend([finger.tip_velocity.x, finger.tip_velocity.y, finger.tip_velocity.z])
                        break
            frame_data = []
            frame_data.extend(finger_pos)
            frame_data.extend(finger_vel)
            frame_data = numpy.matrix(frame_data).transpose()
            
            # send the frame to FramesHandler
            self.fh.add_frame(frame_data)
            
            # record last timestamp
            self.last_time_stamp = frame.timestamp
        else:
            self.count_invalid_frames += 1
            if(self.count_invalid_frames > 3):
                self.fh.reset_memory()
                self.count_invalid_frames = 0
                sys.stdout.write("\n** LSTM cells memory reset **\n")
                sys.stdout.flush()
