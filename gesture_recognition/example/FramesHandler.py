from __future__ import print_function
import numpy
from enum import Enum
from gesture_recognition.mlcore.GestureDetectionPredict import GestureDetectionPredict
from gesture_recognition.mlcore.GestureClassificationPredict import GestureClassificationPredict
import six.moves.cPickle as pickle

class GestureType(Enum):
    """Gesture types"""
    Other = 0
    Circle = 1
    Swipe = 2
    Correct = 3
    Wrong = 4
    Pull = 5
    Push = 6

class FramesHandler(object):
    """
    Handle the Frames sent from Leap Listener.
    """
    
    def __init__(self, paramsFileGD, paramsFileGC):
        """Frames Handler object initialization function"""
        # config
        self.config = pickle.load(open(paramsFileGD + ".pkl"))
        # get the GC & GD objects
        self.gd = GestureDetectionPredict(self.config, paramsFileGD)
        self.gc = GestureClassificationPredict(self.config, paramsFileGC)
        self.startFrames = numpy.zeros((self.config.frame_size, self.config.boundary_inclusion))
        self.gestureFrames = numpy.zeros((1,1))
        self.gestureInProgress = False
        self.gestureEndFramesCount = 0
    
    def add_frame(self, frame):
        """Add the frame to the handler."""
        # send the frame to GD and get the label (frame with/without gesture)
        frameLabel = self.gd.predict_label(frame)
        # add the frame
        if frameLabel==0:
            if self.gestureInProgress==True:
                self.gestureEndFramesCount += 1
                self.gestureFrames = numpy.concatenate((self.gestureFrames, frame), axis=1)
                if self.gestureEndFramesCount >= self.config.frame_labels_patience: # end of gesture
                    # send gesture if it has minimun frames
                    if self.gestureFrames.shape[1] > self.config.gesture_frames_count_threshold:
                        self.gestureFrames = numpy.concatenate((self.startFrames, self.gestureFrames), axis=1)
                        gestureProb = self.gc.predict_prob(self.gestureFrames)
                        gestureLabel = gestureProb.argmax() if gestureProb.max() > 0.96 else 0
                        if gestureLabel != 0:
                            print("%s gesture; %f Probability; %d frames count" 
                                  %(GestureType._value2member_map_[gestureLabel].name,
                                    gestureProb[0][gestureLabel],
                                    self.gestureFrames.shape[1]))
                    # reset params
                    self.gestureInProgress = False
                    self.gestureEndFramesCount = 0
                    self.gestureFrames = numpy.zeros((1,1))
            else:
                # maintain the gesture begin number of frames (excluding the first frame - FIFO)
                self.startFrames = numpy.concatenate((self.startFrames, frame), axis=1)[:,1:]
        elif frameLabel==1:
            self.gestureInProgress = True
            self.gestureEndFramesCount = 0
            if self.gestureFrames.size == 1:
                self.gestureFrames = frame
            else:
                self.gestureFrames = numpy.concatenate((self.gestureFrames, frame), axis=1)
    
    def reset_memory(self):
        """ Reset memory cells in GD """
        self.gd.reset_memory_cells()
