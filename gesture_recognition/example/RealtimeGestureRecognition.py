from __future__ import print_function
import readchar, Leap
from LeapFramesListener import LeapFramesListener

def main():
    controller = Leap.Controller()
    listener = LeapFramesListener()
    controller.add_listener(listener)
    print("Press any key to exit.")
    key = raw_input('') #readchar.readkey()

if __name__ == "__main__":
    main()
