from __future__ import print_function
import numpy, sys

import config
from gesture_recognition.test.GestureDetectionPredict import GestureDetectionPredict
from gesture_recognition.test.GestureClassificationPredict import GestureClassificationPredict

# =========================================================================
# Test Gesture Detection
# =========================================================================

# load the frames
testData = numpy.load("./data/frames_data_no2.npz")['test']
modelFile = "./data/GD_params_run1.npz"

# test model object
gdp = GestureDetectionPredict(config, modelFile)

# iterate for each frame
all_pred_labels = []
for eg_id in range(len(testData[1])):
    gdp.reset_memory_cells()
    pred_labels = []
    for f_id in range(testData[0][eg_id].shape[1]):
        frame = numpy.matrix(testData[0][eg_id][:,f_id]).transpose()
        pred_labels.append(gdp.predict_label(frame))
        #sys.stdout.write(pred_label)
        #sys.stdout.flush()
    all_pred_labels.append(pred_labels)

# save to file
numpy.savez("./data/frames_data_no2_pseudo_realtime_pred_labels.npz", 
            pred_frame_labels=all_pred_labels)

# =========================================================================
# Test Gesture Classification
# =========================================================================

# get data
def get_data(file_name, frame_length):
    """
    Gets the data from the file.

    Parameters
    ----------
    file_name: location of the file.
    frame_length: Length of each frame (frame dimension)

    Return/Output
    -------------
    A tuple. data[0] = list of matrices (examples)
             data[1] = true labels
    """
    fp = open(file_name, 'r')
    example_count = int(fp.readline())
    data = ([], [])
    for line in fp.readlines():
        line = [float(num) for num in line.split()]
        data[1].append(int(line[0]))
        example = numpy.array(line[1:]).reshape((len(line))/frame_length, frame_length)
        data[0].append(example.transpose())
    data = (data[0], numpy.array(data[1]))
    return data

gcTestDataFile = "/home/inblueswithu/Documents/LM_DataCollection/gc_test_data.txt"
gcModelParamsFile = "./data/GC_params_run1.npz"
# gcp object
gcp = GestureClassificationPredict(config, gcModelParamsFile)
# load the data
gcTestData = get_data(gcTestDataFile, config.frame_size)
# iterate on each example
print("Prediction wrong for id: ")
for i in range(len(gcTestData[1])):
    predLabel = gcp.predict_label(gcTestData[0][i])
    if predLabel != gcTestData[1][i]:
        print(i)
    else:
        print('T')
