# GESTURE RECOGNITION USING DEEP LEARNING

Gestures are going to be the future of interaction in different spaces like Virtual Reality,
Holographic displays or Personal Computing. In this arena we are at a very niche stage. 
Detecting and recognizing a gesture is very hard using traditional methods i.e. you will 
have to formulate a mathematical equation for the gesture (really hard for a complex gesture
like Pull, Punch, Infinity etc) and then curve fit the hand motion to verify it. So, I'm 
developing a method using Machine Learning & Deep Learning techniques to quickly create a
new gesture and recognize it when needed. 

This will help tremendously for both application developers and users. Developers can 
easily create a new gesture in matter of days than weeks. Where as users can create their
own gestures and map them to the application if they are not comfortable with the defaults
(e.g., most gamers change the key combinations on keyboard over defaults). We can clearly 
see the possibilities of technology like this. 
I'm using data from Leap Motion controller to achieve it.

### Project Document
I strongly suggest to read this document if you want to understand and implement this
project to recognize custom gestures using Leap Motion frames data.  
`Gesture Recognition using Deep Learning document.pdf`

### Published Paper
**Recognition of Dynamic Hand Gestures from 3D Motion Data using LSTM and CNN architectures**  
by Chinmaya Naguri and Razvan Bunescu  
*Proceedings of the 16th IEEE International Conference on Machine Learning and Applications (ICMLA), Cancun, Mexico, December 2017*  
`icmla17.pdf`

**Note:** Write me an email if you need any clarification or create an issue in the git repo.
I'll see what I can do.

--------------------

###  Package Structure
Below is an introduction to the contents of this python package.

```
gesture_recognition
|-- data_collection
|-- example
    |-- data
    |-- experiments
|-- mlcore de 
```

#### gesture_recognition
This is the root python package. It contains the machine learning code and data collection
code for gesture training, detection & recognition. It also contains example directory,
where all my project data recides. 

I have seperated the Machine Learning source code (mlcore directory) and 
Data collection source code (data_collection directory) on purpose from my project (example 
directory) because it can be used for any kind of data which represents gestures or similar
patterns. With minimal modifications, anyone should be able to kick start their project.

*NOTE:* Ideally a python package should only contain the code needed to do a particular job.
However, I was stupid enough to include my project data and experiments to make it
accessible in a single place. If you are using this package to train and implement your
own gestures, you should probably clone this repo and remove the example directory and 
create your own. It is important to understand what this directory contains.

#### data_collection
The source code for Recording the Leapmotion frames, Annotating the frames (using the 
video recorded simultaneouly) and extracting the data from MongoDB to text files is in
this directory. Like the suggests, it only has source files for data collection.

#### example
Like I mentioned earlier, this directory has all the raw data, multiple experiments,
evaluations, training the ML model, pseudo realtime & realtime gesture recognition
source code. This example directory is what my project and the published paper talks
about.

TODO:
* Write about the DataCollection.py [Step by step procedure for collecting data]

#### example/data
All my project data is here. Here are the terms to make it little easier to understand the 
naming convention: 
* GD --> Gesture Detection
* GC --> Gesture Classification  

These are the two stages in my project to recognize a gesture. For clear understanding, 
please read the complete project document as suggested in the beggining.

#### example/experiments
Different experiments on the collected data. There is no easy way to explain it. It has
source code for multiple experiments and K fold evaluations. I won't be surprized if parts
of the data directories are corrupted. Well, it is part of the experimentation ;)

#### mlcore
This houses the source code for Gesture Detection (GD) & Gesture Classification (GC) for
Leap Motion controller data. Like I mentioned above this code can be used for custom 
gestures and the configuration can be passed using the `config` class (`example/config.py`).
